vendor panel
user panel
admin panel
view profile for user
edit profile for all
Reviews
payments
Subscriptions


Summary:

Referencing previous iteration related to technical perspective, Eventeous is based on Laravel framework & LAMP Development Stack with the languages such as HTML, CSS, Bootstrap, jQuery, JavaScript on Front-end & PHP, MySQL on backend.

As of progress for now, we are done with wrapping up more than 80% of the project
Please see the quick technical progress measure below:

1) Frontend (Pages implemented):
        - Login
        - Register
        - About us
        - Contact us
        - Homepage
        - Dashboard
        - View Profile
        - Edit Profile
        - See all venues, vehicles and decors
        - See single venue, vehicle and decor

2) Backend (functionalities added):

    - Register & login setup for user & vendors
    - Homepage redirections added
    - Vendors can add venues, vehicles and decors
    - Users can search, view & book venues, vehicles and decors
    - Admin can see complete dashboard for users, venues, vehicles, booking, payments and decors

3) Database:
    - Laravel Migrarations added & completed for all tables and columns

4) Remaining items:
    - Admin Operations
    - Payments & Subscriptions
    - Reviews for venues, vehicles and decors
