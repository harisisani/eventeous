<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Models\User;
use Hash;

class AuthController extends Controller
{
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function index()
    {
        if(Auth::check()){
            if(Auth::user()->usertype=="vendor"){
                return view('dashboards.vendor-dashboard');
            }
            if(Auth::user()->usertype=="admin"){
                return view('dashboards.admin');
            }
            if(Auth::user()->usertype=="user"){
                return view('dashboards.user');
            }
        }
        return view('auth.login');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function registration()
    {
        if(Auth::check()){
            if(Auth::user()->usertype=="vendor"){
                return view('dashboards.vendor-dashboard');
            }
            if(Auth::user()->usertype=="admin"){
                return view('dashboards.admin');
            }
            if(Auth::user()->usertype=="user"){
                return view('dashboards.user');
            }
        }

        return view('auth.register');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function postLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->intended('dashboard')
                        ->withSuccess('You have Successfully loggedin');
        }else{
            return "wrong";
        }

        // return redirect("login")->withSuccess('Oppes! You have entered invalid credentials');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function postRegistration(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'email' => 'required|email|unique:users',
            // 'password' => 'required|min:6',
        ]);

        $data = $request->all();
        $check = $this->create($data);
        // return redirect("dashboard")->withSuccess('Great! You have Successfully loggedin');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function dashboard()
    {
        if(Auth::check()){
            if(Auth::user()->usertype=="vendor"){
                return view('dashboards.vendor-dashboard');
            }
            if(Auth::user()->usertype=="admin"){
                return view('dashboards.admin');
            }
            if(Auth::user()->usertype=="user"){
                return view('dashboards.user');
            }
        }

        return redirect("login")->withSuccess('Opps! You do not have access');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function create(array $data)
    {
      return User::create([
        'username' => $data['username'],
        'contact' => $data['contact'],
        'usertype' => $data['usertype'],
        'deleted' => '0',
        'email' => $data['email'],
        'password' => Hash::make($data['password'])
      ]);
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect('login');
    }

    public function deleteUser(Request $request){
        $user = User::where('id', $request['id'])->first();
        $user->deleted="1";
        $user->save();
    }

    public function saveProfile(Request $request){
        $user = User::where('id', $request['id'])->first();
        $user->username=$request['username'];
        $user->contact=$request['contact'];
        if(!empty($request['password'])){
            $user->password=Hash::make($request['password']);
        }
        if($request->hasfile('image'))
        {
            $file = $request->file('image');
            $extention = $file->getClientOriginalExtension();
            $filename = time().'.'.$extention;
            $file->move('uploads/users/', $filename);
            $user->image = $filename;
        }

        $user->save();
        return Redirect('edit-profile/'.$request['id']);
    }
}
