<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Booking;
use App\Models\Subscriptions;


class BookingController extends Controller
{
    public function store(Request $request)
    {
        $subs = Subscriptions::where('deleted', 0)
        ->where('user_id', Auth::user()->id)
        ->get();
        $newSubs = Subscriptions::where('user_id', Auth::user()->id)->first();
        $newSubs->available_connects= (int) $subs[0]['available_connects']-(int) "1";
        $newSubs->save();

        $book = new Booking;
        $book->user_id = Auth::user()->id;
        $book->vendor_id = $request->input('vendor_id');
        $book->booked_id = $request->input('booked_id');
        $book->bookingDescription = $request->input('bookingDescription');
        $book->booking_date = $request->input('bookingDate');
        $book->booking_category = $request->input('booking_category');
        $book->approvalStatus = "no";
        $book->deleted = "0";
        $book->payment_id = "0";
        $book->save();
    }

    public function updateBookings(Request $request){
        $user = Booking::where('id', $request['id'])->first();
        $user->approvalStatus=$request['status'];
        $user->save();
    }
}
