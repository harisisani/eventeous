<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Payments;
use App\Models\Subscriptions;

class ConnectsController extends Controller
{
    public function store(Request $request)
    {
        $payment = new Payments;
        $payment->user_id = Auth::user()->id;
        $payment->name_on_card = $request->input('name_on_card');
        $payment->amount = $request->input('amount');
        $payment->card_number = $request->input('card_number');
        $payment->cvv = $request->input('cvv');
        $payment->method = $request->input('method');
        $payment->expiry = $request->input('expiry');
        $payment->status = "completed";
        $payment->deleted = "0";
        $payment->save();

        $subs = Subscriptions::where('deleted', 0)
                ->where('user_id', Auth::user()->id)
                ->get();
        if(count($subs)>0){
            $newSubs = Subscriptions::where('user_id', Auth::user()->id)->first();
            $newSubs->available_connects=(int) $request->input('available_connects')+ (int) $subs[0]['available_connects'];
            $newSubs->save();
        }else{
            $newSubs= new Subscriptions;
            $newSubs->user_id = Auth::user()->id;
            $newSubs->available_connects = $request->input('available_connects');
            $newSubs->deleted = "0";
            $newSubs->notes = Auth::user()->usertype;
            $newSubs->save();
        }
    }
}
