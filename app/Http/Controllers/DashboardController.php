<?php

namespace App\Http\Controllers;
use App\Models\Booking;
use App\Models\Decors;
use App\Models\Packages;
use App\Models\User;
use App\Models\Vehicle;
use App\Models\Venue;
use App\Models\Subscriptions;
use App\Models\Payments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class DashboardController extends Controller
{
    public function dashboard(Request $request){

        if(Auth::check()){
            if(Auth::user()->usertype=="vendor"){
                $bookings = Booking::where('deleted', '0')
                    ->Where('vendor_id', Auth::user()->id)
                    ->orderBy('id', 'desc')
                    ->get();
                $users = User::where('deleted', '0')
                    ->Where('usertype', 'user')
                    ->orderBy('id', 'desc')
                    ->get();
                $venues = Venue::where('deleted', '0')
                    ->Where('vendors_id', Auth::user()->id)
                    ->orderBy('id', 'desc')
                    ->get();
                $vehicles = Vehicle::where('deleted', '0')
                    ->Where('vendors_id', Auth::user()->id)
                    ->orderBy('id', 'desc')
                    ->get();
                $decors = Decors::where('deleted', '0')
                    ->Where('vendors_id', Auth::user()->id)
                    ->orderBy('id', 'desc')
                    ->get();
                $usersWithDeleted = User::Where('usertype', 'user')
                ->orderBy('id', 'desc')
                    ->get();
                $venuesWithDeleted = Venue::Where('vendors_id', Auth::user()->id)
                ->orderBy('id', 'desc')
                    ->get();
                $vehiclesWithDeleted = Vehicle::Where('vendors_id', Auth::user()->id)
                ->orderBy('id', 'desc')
                    ->get();
                $decorsWithDeleted = Decors::Where('vendors_id', Auth::user()->id)
                ->orderBy('id', 'desc')
                    ->get();
                $subs = Subscriptions::where('user_id', Auth::user()->id)
                ->orderBy('id', 'desc')
                    ->get();
                $payments = Payments::where('user_id', Auth::user()->id)
                ->orderBy('id', 'desc')
                    ->get();
                return view('dashboards.vendor-dashboard',compact('users','bookings','venues','vehicles','decors','subs','usersWithDeleted','venuesWithDeleted','vehiclesWithDeleted','decorsWithDeleted','payments'));
            }
            if(Auth::user()->usertype=="admin"){
                $bookings = Booking::where('deleted', '0')
                ->orderBy('id', 'desc')
                    ->get();
                $users = User::where('deleted', '0')
                    ->Where('usertype', "user")
                    ->orderBy('id', 'desc')
                    ->get();
                $vendors = User::where('deleted', '0')
                    ->Where('usertype', "vendor")
                    ->orderBy('id', 'desc')
                    ->get();
                $venues = Venue::where('deleted', '0')
                ->orderBy('id', 'desc')
                    ->get();
                $vehicles = Vehicle::where('deleted', '0')
                ->orderBy('id', 'desc')
                    ->get();
                $decors = Decors::where('deleted', '0')
                ->orderBy('id', 'desc')
                    ->get();
                $usersWithDeleted = User::orderBy('id', 'desc')->get();
                $venuesWithDeleted = Venue::orderBy('id', 'desc')->get();
                $vehiclesWithDeleted = Vehicle::orderBy('id', 'desc')->get();
                $decorsWithDeleted = Decors::orderBy('id', 'desc')->get();
                $payments = Payments::orderBy('id', 'desc')->get();
                return view('dashboards.admin',compact('bookings','users','vendors','venues','vehicles','decors','usersWithDeleted','venuesWithDeleted','vehiclesWithDeleted','decorsWithDeleted','payments'));
            }
            if(Auth::user()->usertype=="user"){
                $bookings = Booking::where('deleted', '0')
                    ->Where('user_id', Auth::user()->id)
                    ->orderBy('id', 'desc')
                    ->get();
                $usersWithDeleted = User::Where('usertype', 'vendor')
                ->orderBy('id', 'desc')
                    ->get();
                $venuesWithDeleted = Venue::orderBy('id', 'desc')->get();
                $vehiclesWithDeleted = Vehicle::orderBy('id', 'desc')->get();
                $decorsWithDeleted = Decors::orderBy('id', 'desc')->get();
                $subs = Subscriptions::orderBy('id', 'desc')->get();
                $payments = Payments::where('user_id', Auth::user()->id)
                ->orderBy('id', 'desc')
                    ->get();
                return view('dashboards.user',compact('bookings','usersWithDeleted','venuesWithDeleted','vehiclesWithDeleted','decorsWithDeleted','payments'));

            }
        }
        return redirect("login")->withSuccess('Opps! You do not have access');
    }
}
