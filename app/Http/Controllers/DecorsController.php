<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Models\Decors;
use App\Models\User;
use App\Models\Subscriptions;
use App\Models\Reviews;

class DecorsController extends Controller
{
    public function store(Request $request)
    {
        $decor = new Decors;
        $decor->name = $request->input('decorName');
        $decor->description = $request->input('decorDescription');
        $decor->city = $request->input('decorCity');
        $decor->notes = $request->input('decorNotes');
        $decor->price = $request->input('decorPrice');
        $decor->deleted = "0";
        $decor->vendors_id = Auth::user()->id;
        if($request->hasfile('decorImage'))
        {
            $file = $request->file('decorImage');
            $extention = $file->getClientOriginalExtension();
            $filename = time().'.'.$extention;
            $file->move('uploads/decor/', $filename);
            $decor->picture_links = $filename;
        }
        $decor->save();

        $subs = Subscriptions::where('deleted', 0)
        ->where('user_id', Auth::user()->id)
        ->get();
        $newSubs = Subscriptions::where('user_id', Auth::user()->id)->first();
        $newSubs->available_connects= (int) $subs[0]['available_connects']-(int) "1";
        $newSubs->save();
    }

    public function open_decor(Request $request, $id)
    {
        $decor = Decors::where('deleted', 0)
            ->where('id', $id)
            ->get();
        $users = User::where('deleted', 0)
            ->orderBy('id', 'desc')
            ->get();
            $subs=array();
        if(Auth::check()){
        $subs = Subscriptions::where('deleted', 0)
                ->where('user_id', Auth::user()->id)
                ->get();
        }
        $reviews = Reviews::where('deleted', 0)
                ->where('category', 'decor')
                ->where('categoryId', $id)
                ->orderBy('id', 'desc')
                ->get();
        if(count($decor)>0){
            return view('search-decor',compact('decor','users','subs','reviews'));
        }else{
            return redirect('/decors');
        }
    }

    public function open_all_decors()
    {
        $allDecors = Decors::where('deleted', 0)
        ->orderBy('name', 'desc')
        ->get();
        $users = User::where('deleted', 0)
        ->orderBy('id', 'desc')
        ->get();
        return view('decors',compact('allDecors','users'));
    }

    public function delete(Request $request){
        $venue = Decors::where('id', $request['id'])->first();
        $venue->deleted="1";
        $venue->save();
    }
}
