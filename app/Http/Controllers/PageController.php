<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Decors;
use App\Models\Packages;
use App\Models\User;
use App\Models\Subscriptions;
use App\Models\Vehicle;
use App\Models\Venue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{

    public function index(Request $request)
    {
        $users = DB::table('users')
        ->where('deleted', 0)
            ->get();
        $popularVenues = Venue::where('deleted', 0)
            ->orderBy('name', 'desc')
            ->get();
        $popularVehicles = Vehicle::where('deleted', 0)
            ->orderBy('name', 'desc')
            ->get();
        $popularDecors = Decors::where('deleted', 0)
            ->orderBy('name', 'desc')
            ->get();
        $venues = DB::table('venues as v')
            ->where('v.deleted', 0)
            ->select('v.*')
            ->orderBy('v.id', 'desc')
            ->limit(3)
            ->get();
        $vehicles = Vehicle::where('deleted', 0)
            ->orderBy('id', 'desc')
            ->limit(3)
            ->get();
        $decors = Decors::where('deleted', 0)
            ->orderBy('id', 'desc')
            ->limit(3)
            ->get();
        return view('index',compact('users','popularVenues','popularVehicles','popularDecors','venues','vehicles','decors'));
    }

    public function aboutUs(Request $request)
    {
        return view('about-us');
    }
    public function services(Request $request)
    {
        return view('services');
    }

    public function viewProfile(Request $request, $id)
    {
        $users = User::where('deleted', 0)
                ->where('id', $id)
                ->get();
        if(count($users)>0){
            $allvenues = Venue::where('deleted', 0)
                ->get();
            $allvehicles = Vehicle::where('deleted', 0)
                ->get();
            $alldecors = Decors::where('deleted', 0)
                ->get();
            $bookings = Booking::where('deleted', 0)
                ->where('user_id', $id)
                ->get();
            $venues = Venue::where('deleted', 0)
                ->where('vendors_id', $id)
                ->get();
            $vehicles = Vehicle::where('deleted', 0)
                ->where('vendors_id', $id)
                ->get();
            $decors = Decors::where('deleted', 0)
                ->where('vendors_id', $id)
                ->get();
            return view('dashboards.view-profile',compact('bookings','allvenues','allvehicles','alldecors','users','venues','vehicles','decors'));
        }else{
            return redirect('/');
        }
    }

    public function editProfile(Request $request)
    {
        if(Auth::check()){
            $users = User::where('deleted', 0)
            ->where('id', Auth::user()->id)
            ->get();
            return view('dashboards.edit-profile',compact('users'));
        }else{
            return redirect('/');
        }

    }
    public function contactUs(Request $request)
    {
        return view('contact-us');
    }
    public function packages(Request $request)
    {
        $subs = Subscriptions::where('deleted', 0)
                ->where('user_id', Auth::user()->id)
                ->get();
        return view('packages',compact('subs'));
    }
    public function comingsoon(Request $request)
    {
        return view('comingsoon');
    }

    public function login(Request $request)
    {
        return view('login');
    }

    public function register(Request $request)
    {
        return view('register');
    }


    public function run_search(Request $request)
    {
        $category=trim(strtolower($request->input('searchCategory')));
        $keyword =trim(strtolower($request->input('searchKeyword')));
        $city =trim(strtolower($request->input('searchCity')));
            if($category=="venues"){
                $allVenues = Venue::
                where('deleted', 0)
                ->where('name', 'LIKE', "%{$keyword}%")
                ->where('city', $city)
                ->orderBy('id', 'desc')
                ->get();
                $users = User::where('deleted', 0)
                ->orderBy('id', 'desc')
                ->get();
                return view('venues',compact('allVenues','users'));
            }
            if($category=="vehicles"){
                $allVehicles = Vehicle::where('deleted', 0)
                ->where('city', $city)
                ->where('name', 'LIKE', "%{$keyword}%")
                ->orderBy('name', 'desc')
                ->get();
                $users = User::where('deleted', 0)
                ->orderBy('id', 'desc')
                ->get();
                return view('vehicles',compact('allVehicles','users'));
            }
            if($category=="decors"){
                $allDecors = Decors::where('deleted', 0)
                ->where('city', $city)
                ->where('name', 'LIKE', "%{$keyword}%")
                ->orderBy('name', 'desc')
                ->get();
                $users = User::where('deleted', 0)
                ->orderBy('id', 'desc')
                ->get();
                return view('decors',compact('allDecors','users'));
            }

    }
}
