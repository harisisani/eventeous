<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Reviews;
class ReviewsController extends Controller
{
    public function store(Request $request)
    {
        $review = new Reviews;
        $review->user_id = Auth::user()->id;
        $review->description = $request->input('description');
        $review->category = $request->input('category');
        $review->categoryId = $request->input('categoryId');
        $review->notes = "added by customer";
        $review->deleted = "0";
        $review->save();
    }
}
