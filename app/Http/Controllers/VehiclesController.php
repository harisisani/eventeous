<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Models\Vehicle;
use App\Models\User;
use App\Models\Subscriptions;
use App\Models\Reviews;
class VehiclesController extends Controller
{
    public function store(Request $request)
    {
        $vehicle = new Vehicle;
        $vehicle->name = $request->input('vehicleName');
        $vehicle->description = $request->input('vehicleDescription');
        $vehicle->city = $request->input('vehicleCity');
        $vehicle->notes = $request->input('vehicleNotes');
        $vehicle->price = $request->input('vehiclePrice');
        $vehicle->deleted = "0";
        $vehicle->vendors_id = Auth::user()->id;
        if($request->hasfile('vehicleImage'))
        {
            $file = $request->file('vehicleImage');
            $extention = $file->getClientOriginalExtension();
            $filename = time().'.'.$extention;
            $file->move('uploads/vehicles/', $filename);
            $vehicle->picture_links = $filename;
        }
        $vehicle->save();

        $subs = Subscriptions::where('deleted', 0)
        ->where('user_id', Auth::user()->id)
        ->get();
        $newSubs = Subscriptions::where('user_id', Auth::user()->id)->first();
        $newSubs->available_connects= (int) $subs[0]['available_connects']-(int) "1";
        $newSubs->save();
    }

    public function open_vehicle(Request $request, $id)
    {
        $vehicle = Vehicle::where('deleted', 0)
            ->where('id', $id)
            ->get();
        $users = User::where('deleted', 0)
            ->orderBy('id', 'desc')
            ->get();
            $subs=array();
        if(Auth::check()){
        $subs = Subscriptions::where('deleted', 0)
                ->where('user_id', Auth::user()->id)
                ->get();
        }
        $reviews = Reviews::where('deleted', 0)
                ->where('category', 'vehicle')
                ->where('categoryId', $id)
                ->orderBy('id', 'desc')
                ->get();
        if(count($vehicle)>0){
            return view('search-vehicle',compact('vehicle','users','subs','reviews'));
        }else{
            return redirect('/vehicles');
        }
    }

    public function open_all_vehicles()
    {
        $allVehicles = Vehicle::where('deleted', 0)
        ->orderBy('name', 'desc')
        ->get();
        $users = User::where('deleted', 0)
        ->orderBy('id', 'desc')
        ->get();
        return view('vehicles',compact('allVehicles','users'));
    }

    public function delete(Request $request){
        $venue = Vehicle::where('id', $request['id'])->first();
        $venue->deleted="1";
        $venue->save();
    }
}
