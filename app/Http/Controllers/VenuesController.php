<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Models\Venue;
use App\Models\User;
use App\Models\Subscriptions;
use App\Models\Reviews;
class VenuesController extends Controller
{
    public function store(Request $request)
    {
        $venue = new Venue;
        $venue->name = $request->input('venueName');
        $venue->description = $request->input('venueDescription');
        $venue->city = $request->input('venueCity');
        $venue->notes = $request->input('venueNotes');
        $venue->price = $request->input('venuePrice');
        $venue->deleted = "0";
        $venue->vendors_id = Auth::user()->id;
        if($request->hasfile('venueImage'))
        {
            $file = $request->file('venueImage');
            $extention = $file->getClientOriginalExtension();
            $filename = time().'.'.$extention;
            $file->move('uploads/venues/', $filename);
            $venue->picture_links = $filename;
        }
        $venue->save();

        $subs = Subscriptions::where('deleted', 0)
        ->where('user_id', Auth::user()->id)
        ->get();
        $newSubs = Subscriptions::where('user_id', Auth::user()->id)->first();
        $newSubs->available_connects= (int) $subs[0]['available_connects']-(int) "1";
        $newSubs->save();
    }

    public function open_venue(Request $request, $id)
    {
        $venue = Venue::where('deleted', 0)
            ->where('id', $id)
            ->get();
        $users = User::where('deleted', 0)
        ->orderBy('id', 'desc')
        ->get();
        $subs=array();
        if(Auth::check()){
        $subs = Subscriptions::where('deleted', 0)
                ->where('user_id', Auth::user()->id)
                ->get();
        }
        $reviews = Reviews::where('deleted', 0)
                ->where('category', 'venue')
                ->where('categoryId', $id)
                ->orderBy('id', 'desc')
                ->get();
        if(count($venue)>0){
            return view('search-venue',compact('venue','users','subs','reviews'));
        }else{
            return redirect('/venues');
        }
    }

    public function open_all_venues()
    {
        $allVenues = Venue::where('deleted', 0)
        ->orderBy('id', 'desc')
        ->get();
        $users = User::where('deleted', 0)
        ->orderBy('id', 'desc')
        ->get();
        return view('venues',compact('allVenues','users'));
    }

    public function delete(Request $request){
        $venue = Venue::where('id', $request['id'])->first();
        $venue->deleted="1";
        $venue->save();
    }
}
