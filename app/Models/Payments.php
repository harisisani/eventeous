<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    use HasFactory;
    protected $table='payments';
    protected $fillable=[
        'user_id',
        'name_on_card',
        'amount',
        'card_number',
        'cvv',
        'status',
        'expiry',
        'method',
        'deleted',
    ];
}
