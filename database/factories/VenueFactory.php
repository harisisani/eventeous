<?php

namespace Database\Factories;

use App\Models\Venue;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class VenueFactory extends Factory
{

    protected $model = Venue::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement(['Sapphire Hall', 'Galaxy Hall','Diamond Hall']),  // Random task title
            'description' => $this->faker->text(), // Random task description
            'price'=> "$".$this->faker->numberBetween(1500, 2000),
            'picture_links'=>$this->faker->randomElement(['http://127.0.0.1:8000/assets/images/venues/1.jpg', 'http://127.0.0.1:8000/assets/images/venues/2.jpg','http://127.0.0.1:8000/assets/images/venues/3.jpg','Islamabad','Attock']),
            'city'=>$this->faker->randomElement(['Lahore', 'Karachi','Rawalpindi','Islamabad','Attock']),
            'notes'=>$this->faker->paragraph(),
            'vendors_id'=>$this->faker->numberBetween(1,10),
            'deleted'=>0
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
