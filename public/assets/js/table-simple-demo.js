window.addEventListener('DOMContentLoaded', event => {
    // Simple-DataTables
    // https://github.com/fiduswriter/Simple-DataTables/wiki

    const datatablesSimple = document.getElementById('usertable');
    if (datatablesSimple) {
        new simpleDatatables.DataTable(datatablesSimple);
    }
    const datatablesSimple2 = document.getElementById('vendortable');
    if (datatablesSimple2) {
        new simpleDatatables.DataTable(datatablesSimple2);
    }
    const datatablesSimple3 = document.getElementById('venuetable');
    if (datatablesSimple3) {
        new simpleDatatables.DataTable(datatablesSimple3);
    }
    const datatablesSimple4 = document.getElementById('vehicletable');
    if (datatablesSimple4) {
        new simpleDatatables.DataTable(datatablesSimple4);
    }
    const datatablesSimple5 = document.getElementById('bookingtable');
    if (datatablesSimple5) {
        new simpleDatatables.DataTable(datatablesSimple5);
    }
    const datatablesSimple6 = document.getElementById('decortable');
    if (datatablesSimple6) {
        new simpleDatatables.DataTable(datatablesSimple6);
    }
    const datatablesSimple7 = document.getElementById('bookingTable');
    if (datatablesSimple7) {
        new simpleDatatables.DataTable(datatablesSimple7);
    }
    const datatablesSimple8 = document.getElementById('paymenttable');
    if (datatablesSimple8) {
        new simpleDatatables.DataTable(datatablesSimple8);
    }

});
