<!doctype html>
<html class="no-js" lang=""> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Eventeous</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="{{ url('/') }}/assets/apple-touch-icon.png">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/normalize.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/icomoon.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/owl.carousel.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap-select.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/scrollbar.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/jquery.mmenu.all.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/prettyPhoto.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/transitions.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/color.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/responsive.css">
	<script src="{{ url('/') }}/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
		@include('header')
	
		<!--************************************
				Inner Banner Start
		*************************************-->
		<section class="tg-parallax tg-innerbanner" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ url('/') }}/assets/images/parallax/bgparallax-05.jpg">
			<div class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h1>About Us</h1>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--************************************
				Inner Banner End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="tg-main" class="tg-main tg-haslayout">
			<!--************************************
					Features Start
			*************************************-->
			<section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="tg-features">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="tg-feature">
								<div class="tg-featuretitle">
										<h2><span>01</span>What is Eventeous</h2>
									</div>
									<div class="tg-description">
										<p>Website for an online booking system to arrange the necessities for an event.</p>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<div class="tg-feature">
									<div class="tg-featuretitle">
										<h2><span>02</span>Subscription</h2>
									</div>
									<div class="tg-description">
										<p>We offer monthly-based subscription for our users.</p>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<div class="tg-feature">
									<div class="tg-featuretitle">
										<h2><span>03</span>Service Provider</h2>
									</div>
									<div class="tg-description">
										<p>We are providing top service providers.</p>
									</div>
								</div>
							</div>
							<!-- <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<div class="tg-feature">
									<div class="tg-featuretitle">
										<h2><span>04</span><a href="{{ url('/') }}/assets/javascript:void(0);">Luxury Hotels</a></h2>
									</div>
									<div class="tg-description">
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh tempor cum soluta nobis consectetuer nihil imperdiet doming...</p>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<div class="tg-feature">
									<div class="tg-featuretitle">
										<h2><span>05</span><a href="{{ url('/') }}/assets/javascript:void(0);">Tourist Guide</a></h2>
									</div>
									<div class="tg-description">
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh tempor cum soluta nobis consectetuer nihil imperdiet doming...</p>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<div class="tg-feature">
									<div class="tg-featuretitle">
										<h2><span>06</span><a href="{{ url('/') }}/assets/javascript:void(0);">Flights Tickets</a></h2>
									</div>
									<div class="tg-description">
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh tempor cum soluta nobis consectetuer nihil imperdiet doming...</p>
									</div>
								</div>
							</div> -->
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					Features End
			*************************************-->
		
			<!--************************************
					Our Guides Start
			*************************************-->
			<!-- <section class="tg-sectionspace tg-haslayout tg-bglight">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="tg-sectionhead">
								<div class="tg-sectiontitle">
									<h2>Meet The Guides</h2>
								</div>
								<div class="tg-description">
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam consectetuer.</p>
								</div>
							</div>
							<div id="tg-guidesslider" class="tg-guidesslider tg-guides owl-carousel">
								<div class="item tg-guide">
									<figure><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/Guides/img-01.jpg" alt="image destination"></a></figure>
									<div class="tg-guidecontent">
										<div class="tg-guidecontenthead">
											<h3><a href="{{ url('/') }}/assets/javascript:void(0);">Martin Blake</a></h3>
											<h4><a href="{{ url('/') }}/assets/javascript:void(0);">Adventure Master</a></h4>
											<ul class="tg-socialicons">
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-05.png" alt="image destinations"></a></li>
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-06.png" alt="image destinations"></a></li>
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-07.png" alt="image destinations"></a></li>
											</ul>
										</div>
										<div class="tg-description">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
										</div>
									</div>
								</div>
								<div class="item tg-guide">
									<figure><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/Guides/img-02.jpg" alt="image destination"></a></figure>
									<div class="tg-guidecontent">
										<div class="tg-guidecontenthead">
											<h3><a href="{{ url('/') }}/assets/javascript:void(0);">Martin Blake</a></h3>
											<h4><a href="{{ url('/') }}/assets/javascript:void(0);">Adventure Master</a></h4>
											<ul class="tg-socialicons">
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-05.png" alt="image destinations"></a></li>
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-06.png" alt="image destinations"></a></li>
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-07.png" alt="image destinations"></a></li>
											</ul>
										</div>
										<div class="tg-description">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
										</div>
									</div>
								</div>
								<div class="item tg-guide">
									<figure><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/Guides/img-03.jpg" alt="image destination"></a></figure>
									<div class="tg-guidecontent">
										<div class="tg-guidecontenthead">
											<h3><a href="{{ url('/') }}/assets/javascript:void(0);">Martin Blake</a></h3>
											<h4><a href="{{ url('/') }}/assets/javascript:void(0);">Adventure Master</a></h4>
											<ul class="tg-socialicons">
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-05.png" alt="image destinations"></a></li>
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-06.png" alt="image destinations"></a></li>
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-07.png" alt="image destinations"></a></li>
											</ul>
										</div>
										<div class="tg-description">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
										</div>
									</div>
								</div>
								<div class="item tg-guide">
									<figure><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/Guides/img-01.jpg" alt="image destination"></a></figure>
									<div class="tg-guidecontent">
										<div class="tg-guidecontenthead">
											<h3><a href="{{ url('/') }}/assets/javascript:void(0);">Martin Blake</a></h3>
											<h4><a href="{{ url('/') }}/assets/javascript:void(0);">Adventure Master</a></h4>
											<ul class="tg-socialicons">
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-05.png" alt="image destinations"></a></li>
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-06.png" alt="image destinations"></a></li>
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-07.png" alt="image destinations"></a></li>
											</ul>
										</div>
										<div class="tg-description">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
										</div>
									</div>
								</div>
								<div class="item tg-guide">
									<figure><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/Guides/img-02.jpg" alt="image destination"></a></figure>
									<div class="tg-guidecontent">
										<div class="tg-guidecontenthead">
											<h3><a href="{{ url('/') }}/assets/javascript:void(0);">Martin Blake</a></h3>
											<h4><a href="{{ url('/') }}/assets/javascript:void(0);">Adventure Master</a></h4>
											<ul class="tg-socialicons">
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-05.png" alt="image destinations"></a></li>
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-06.png" alt="image destinations"></a></li>
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-07.png" alt="image destinations"></a></li>
											</ul>
										</div>
										<div class="tg-description">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
										</div>
									</div>
								</div>
								<div class="item tg-guide">
									<figure><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/Guides/img-03.jpg" alt="image destination"></a></figure>
									<div class="tg-guidecontent">
										<div class="tg-guidecontenthead">
											<h3><a href="{{ url('/') }}/assets/javascript:void(0);">Martin Blake</a></h3>
											<h4><a href="{{ url('/') }}/assets/javascript:void(0);">Adventure Master</a></h4>
											<ul class="tg-socialicons">
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-05.png" alt="image destinations"></a></li>
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-06.png" alt="image destinations"></a></li>
												<li><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/icons/icon-07.png" alt="image destinations"></a></li>
											</ul>
										</div>
										<div class="tg-description">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> -->
			<!--************************************
					Our Guides End
			*************************************-->
			<!--************************************
					About Us Start
			*************************************-->
			<section class="tg-aboutus">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="row">
						<figure data-vide-bg="poster: {{ url('/') }}/assets/images/destination/img-01.jpg" data-vide-options="position: 0% 50%"></figure>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="row">
						<div class="tg-textbox">
							<div class="tg-sectiontitle">
								<h2>A Little About Us</h2>
							</div>
							<div class="tg-description">
								<p>The Eventeous came forward as a rescue to users that are in search of an easy-going online platform to form events dimensions. It is a public responsive & dynamic web portal application that is designed & developed to provide the customer an ease of access to book any thing related to events such as venues, logistics, vendors etc. The application is developed with an attractive User Interface as well as easy-going User Experience.</p>
								<p>There are several websites & services that were created to cater to this problem but almost all of them are a bit costly & difficult to use, that became the reason to form Eventeous.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-right">
					<div class="row">
						<figure data-vide-bg="poster: {{ url('/') }}/assets/images/parallax/bgparallax-01.jpg" data-vide-options="position: 0% 50%"></figure>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-left">
					<div class="row">
						<div class="tg-textbox">
							<div class="tg-sectiontitle">
								<h2>A Little About Us</h2>
							</div>
							<div class="tg-description">
								<p>The Eventeous is an effective web-based system. It overcomes the hassle of searching venues, transport programs, vendors, etc & provide you a source of connection with an ease of access to the available options with just a fingers tip away.</p>
								<p>Eventeous is a full-fledged dynamic & responsive web system for vendors, customers, venue owners, transporters & all other who has something to provide as a service or rental exposures.</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					About Us End
			*************************************-->
			<!--************************************
					FAQs Start
			*************************************-->
			<!--************************************
					FAQs End
			*************************************-->
			<!--************************************
					Our Partners Start
			*************************************-->
			<!-- <section class="tg-parallax" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ url('/') }}/assets/images/parallax/bgparallax-03.jpg">
				<div class="tg-sectionspace tg-haslayout">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="tg-ourpartners">
									<div class="tg-pattern"><img src="{{ url('/') }}/assets/images/patternw.png" alt="image destination"></div>
									<h2>Our Partners</h2>
									<ul class="tg-partners">
										<li><figure><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/partners/img-01.png" alt="image destinations"></a></figure></li>
										<li><figure><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/partners/img-02.png" alt="image destinations"></a></figure></li>
										<li><figure><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/partners/img-03.png" alt="image destinations"></a></figure></li>
										<li><figure><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/partners/img-04.png" alt="image destinations"></a></figure></li>
										<li><figure><a href="{{ url('/') }}/assets/javascript:void(0);"><img src="{{ url('/') }}/assets/images/partners/img-05.png" alt="image destinations"></a></figure></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> -->
			<!--************************************
					Our Partners End
			*************************************-->
		</main>
		<!--************************************
				Main End
		*************************************-->
		<!--************************************
				Footer Start
		*************************************-->
		<footer id="tg-footer" class="tg-footer tg-haslayout">
			
			<div class="tg-footerbar">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p>Copyright &copy; 2022 Eventeous. All  rights reserved</p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--************************************
				Footer End
		*************************************-->
	</div>
	<!--************************************
			Wrapper End
	*************************************-->
	<!--************************************
			Search Start
	*************************************-->
	<div id="tg-search" class="tg-search">
		<button type="button" class="close"><i class="icon-cross"></i></button>
		<form>
			<fieldset>
				<div class="form-group">
					<input type="search" name="search" class="form-control" value="" placeholder="search here">
					<button type="submit" class="tg-btn"><span class="icon-search2"></span></button>
				</div>
			</fieldset>
		</form>
		<ul class="tg-destinations">
			<li>
				<a href="{{ url('/') }}/assets/javascript:void(0);">
					<h3>Europe</h3>
					<em>(05)</em>
				</a>
			</li>
			<li>
				<a href="{{ url('/') }}/assets/javascript:void(0);">
					<h3>Africa</h3>
					<em>(15)</em>
				</a>
			</li>
			<li>
				<a href="{{ url('/') }}/assets/javascript:void(0);">
					<h3>Asia</h3>
					<em>(12)</em>
				</a>
			</li>
			<li>
				<a href="{{ url('/') }}/assets/javascript:void(0);">
					<h3>Oceania</h3>
					<em>(02)</em>
				</a>
			</li>
			<li>
				<a href="{{ url('/') }}/assets/javascript:void(0);">
					<h3>North America</h3>
					<em>(08)</em>
				</a>
			</li>
		</ul>
	</div>
	<!--************************************
			Search End
	*************************************-->

	<script src="{{ url('/') }}/assets/js/vendor/jquery-library.js"></script>
	<script src="{{ url('/') }}/assets/js/vendor/bootstrap.min.js"></script>
	<script src="{{ url('/') }}/assets/https://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&language=en"></script>
	<script src="{{ url('/') }}/assets/js/bootstrap-select.min.js"></script>
	<script src="{{ url('/') }}/assets/js/jquery-scrolltofixed.js"></script>
	<script src="{{ url('/') }}/assets/js/owl.carousel.min.js"></script>
	<script src="{{ url('/') }}/assets/js/jquery.mmenu.all.js"></script>
	<script src="{{ url('/') }}/assets/js/packery.pkgd.min.js"></script>
	<script src="{{ url('/') }}/assets/js/jquery.vide.min.js"></script>
	<script src="{{ url('/') }}/assets/js/scrollbar.min.js"></script>
	<script src="{{ url('/') }}/assets/js/prettyPhoto.js"></script>
	<script src="{{ url('/') }}/assets/js/countdown.js"></script>
	<script src="{{ url('/') }}/assets/js/parallax.js"></script>
	<script src="{{ url('/') }}/assets/js/gmap3.js"></script>
	<script src="{{ url('/') }}/assets/js/main.js"></script>
</body>
</html>