
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/assets/login-register/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{ url('/') }}/assets/login-register/util.css">
	<link rel="stylesheet" type="text/css" href="{{ url('/') }}/assets/login-register/main.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/assets/login-register/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body style="background: url({{ url('/') }}/assets/images/bg-login.jpg) center;">
	<div class="limiter">
		<div class="container-login100">
			<div style="background:white;" class="wrap-login100">
				<form  class="login100-form">
					<span class="login100-form-title p-b-26">
						Login
					</span>
					<span class="login100-form-title p-b-48">
						<img src="{{ url('/') }}/assets/images/logo.jpg" alt="logo" style="max-height:100px;">
					</span>

					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="email">
						<span class="focus-input100" data-placeholder="Email"></span>
					</div>

					<div class="wrap-input100 validate-input">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="password">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>


					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<butt class="login100-form-btn btn-submit">
								Login
							</butt>
						</div>
					</div>

					<div class="text-center p-t-20">
						<span class="txt1">
							Don’t have an account?
						</span>
						<a class="txt2" href="{{ url('/') }}/register">
							Register
						</a>
					</div>
					<div class="text-center p-t-20">
						<span class="txt1">
							Want to see our home instead?
						</span>
						<a class="txt2" href="{{ url('/') }}">
							Home
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="dropDownSelect1"></div>

	<script src="{{ url('/') }}/assets/login-register/jquery-3.2.1.min.js"></script>
	<script src="{{ url('/') }}/assets/login-register/bootstrap.min.js"></script>
	<script src="{{ url('/') }}/assets/login-register/main.js"></script>
	<script src="{{ url('/') }}/assets/login-register/poppers.js"></script>
	<script src="{{ url('/') }}/assets/login-register/sweetalert2@11.js"></script>
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
</body>
</html>
<script type="text/javascript">
   
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   
    $(".btn-submit").click(function(e){
        e.preventDefault();
        var email = $("input[name=email]").val();
        var password = $("input[name=password]").val();
		if(email=="" || email==""){
			Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Enter the email & password'
			})
		}else{
			var contact = $("input[name=contact]").val();
			var usertype = $("select[name=usertype]").val();
			$.ajax({
			   type:'POST',
			   url:"{{ route('login.post') }}",
			   data:{password:password, email:email},
			   success:function(data){
				if(data=="wrong"){
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: 'Incorrect Credentials'
						})
				}else{
					window.location = "{{ route('dashboard') }}";
				}
			   },
			   error: function (error) {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Network Error'
					})
				}
			});
		}
    });
</script>