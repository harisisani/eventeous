
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Register</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/assets/login-register/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{ url('/') }}/assets/login-register/util.css">
	<link rel="stylesheet" type="text/css" href="{{ url('/') }}/assets/login-register/main.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/assets/login-register/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body style="background: url({{ url('/') }}/assets/images/bg-login.jpg) center;">

	<div class="limiter">
		<div class="container-login100">
			<div style="background:white;" class="wrap-login100">
				<form class="login100-form validate-form">
				@csrf
					<span class="login100-form-title p-b-26">
						Register
					</span>
					<span class="login100-form-title p-b-48">
						<img src="{{ url('/') }}/assets/images/logo.jpg" alt="logo" style="max-height:100px;">

					<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
						<input class="input100" type="text" name="email">
						<span class="focus-input100" data-placeholder="Email*"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="password">
						<span class="focus-input100" data-placeholder="Password*"></span>
					</div>

                    <div class="wrap-input100">
						<input class="input100" type="text" name="username">
						<span class="focus-input100" data-placeholder="Full Name*"></span>
					</div>

                    <div class="wrap-input100">
						<input class="input100" type="text" name="contact">
						<span class="focus-input100" data-placeholder="Contact*"></span>
					</div>
                    <div class="wrap-input100">
                        <select class="input100" style="width:100%;border: none;" name="usertype" id="">
                            <option selected value="user">User</option>
                            <option value="vendor">Vendor</option>
                        </select>
					</div>
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn btn-submit">
								Register
							</button>
						</div>
					</div>

					<div class="text-center p-t-20">
						<span class="txt1">
							Already have an account?
						</span>

						<a class="txt2" href="{{ url('/') }}/login">
							Login
						</a>
					</div>
					<div class="text-center p-t-20">
						<span class="txt1">
							Want to see our home instead?
						</span>
						<a class="txt2" href="{{ url('/') }}">
							Home
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

	<script src="{{ url('/') }}/assets/login-register/jquery-3.2.1.min.js"></script>
	<script src="{{ url('/') }}/assets/login-register/bootstrap.min.js"></script>
	<script src="{{ url('/') }}/assets/login-register/main.js"></script>
	<script src="{{ url('/') }}/assets/login-register/poppers.js"></script>
	<script src="{{ url('/') }}/assets/login-register/sweetalert2@11.js"></script>
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
</body>
</html>
<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".btn-submit").click(function(e){
        e.preventDefault();
        var email = $("input[name=email]").val();
        var username = $("input[name=username]").val();
        var password = $("input[name=password]").val();
		if(email=="" || username=="" || password=="" || contact==""){
			Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Fields with * are required'
			})
		}else if(password.length<6){
			Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: 'Password should be atleast 6 characters'
			})
		}else{
			var contact = $("input[name=contact]").val();
			var usertype = $("select[name=usertype]").val();
			$.ajax({
			   type:'POST',
			   url:"{{ route('register.post') }}",
			   data:{username:username, password:password, email:email, contact:contact, usertype:usertype},
			   success:function(data){
				Swal.fire({
						icon: 'success',
						title: 'Good job!',
						text: 'Account created successfully'
					})
                    location.reload();
			   },
			   error: function (error) {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Email already taken'
					})
				}
			});
		}
    });
</script>
