<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Eventeous</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="{{ url('/') }}/assets/apple-touch-icon.png">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/normalize.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/icomoon.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/owl.carousel.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap-select.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/scrollbar.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/jquery.mmenu.all.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/prettyPhoto.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/transitions.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/color.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/responsive.css">
	<script src="{{ url('/') }}/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
		@include('header')
		<!--************************************
				Inner Banner Start
		*************************************-->
		<section class="tg-parallax tg-innerbanner" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ url('/') }}/assets/images/parallax/bgparallax-05.jpg">
			<div class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h1>Contact Us</h1>
							<!-- <h2>We are excited to hear from you</h2> -->
							<!-- <ol class="tg-breadcrumb">
								<li><a href="{{ url('/') }}/assets/javascript:void(0);">Home</a></li>
								<li class="tg-active">Contact Us</li>
							</ol> -->
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--************************************
				Inner Banner End
		*************************************-->
		<main id="tg-main" class="tg-main tg-haslayout">
			<div class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div id="tg-content" class="tg-content">
								<div class="tg-billingdetail">
									<form class="tg-formtheme tg-formbillingdetail">
										<fieldset>
											<div class="tg-bookingdetail" style="width: 100%;">
												<div class="tg-box">
													<div class="tg-heading">
														<h3>Contact Us</h3>
													</div>
													<div class="clearfix"></div>
													<div class="row">
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
															<div class="form-group">
																<label>First name <sup>*</sup></label>
																<input type="text" name="firstname" class="form-control" placeholder="">
															</div>
														</div>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
															<div class="form-group">
																<label>Last name <sup>*</sup></label>
																<input type="text" name="lastname" class="form-control" placeholder="">
															</div>
														</div>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
															<div class="form-group">
																<label>Email Address <sup>*</sup></label>
																<input type="email" name="email" class="form-control" placeholder="">
															</div>
														</div>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
															<div class="form-group">
																<label>Phone Number <sup>*</sup></label>
																<input type="text" name="phonename" class="form-control" placeholder="">
															</div>
														</div>
														<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<div class="form-group">
																<label>Comment <sup>*</sup></label>
																<textarea name="comment" class="form-control" placeholder=""></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										
										</fieldset>
										
										<fieldset>
											<button style="float:right;" class="tg-btn" type="submit"><span>Send</span></button>
										</fieldset>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!--************************************
				Main Start
		*************************************-->
		<main id="tg-main" class="tg-main tg-sectionspace tg-haslayout">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div id="tg-content" class="tg-content">
							<ul class="tg-contactinfo">
								<li>
									<span class="tg-contactinfoicon"><i class="fa fa-commenting-o"></i></span>
									<h2>Get in Touch</h2>
									<span>Telephone: +12 123 1234</span>
									<span>Mobile: +12 123 1234</span>
									<strong><a href="{{ url('/') }}/assets/mailto:hello@Travleu.com">hello@eventeous.com</a></strong>
								</li>
								<li>
									<span class="tg-contactinfoicon"><i class="icon-map-marker"></i></span>
									<h2>Visit Our Location</h2>
									<address>Karachi, Pakistan</address>
									<strong><a href="{{ url('/') }}/assets/javascript:void(0);">Get Directions</a></strong>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!--************************************
				Main End
		*************************************-->
		<!--************************************
				Footer Start
		*************************************-->
		<footer id="tg-footer" class="tg-footer tg-haslayout">
			
			<div class="tg-footerbar">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p>Copyright &copy; 2022 Eventeous. All  rights reserved</p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--************************************
				Footer End
		*************************************-->
	</div>
	<!--************************************
			Wrapper End
	*************************************-->
	<!--************************************
			Search Start
	*************************************-->
	<div id="tg-search" class="tg-search">
		<button type="button" class="close"><i class="icon-cross"></i></button>
		<form>
			<fieldset>
				<div class="form-group">
					<input type="search" name="search" class="form-control" value="" placeholder="search here">
					<button type="submit" class="tg-btn"><span class="icon-search2"></span></button>
				</div>
			</fieldset>
		</form>
		<ul class="tg-destinations">
			<li>
				<a href="{{ url('/') }}/assets/javascript:void(0);">
					<h3>Europe</h3>
					<em>(05)</em>
				</a>
			</li>
			<li>
				<a href="{{ url('/') }}/assets/javascript:void(0);">
					<h3>Africa</h3>
					<em>(15)</em>
				</a>
			</li>
			<li>
				<a href="{{ url('/') }}/assets/javascript:void(0);">
					<h3>Asia</h3>
					<em>(12)</em>
				</a>
			</li>
			<li>
				<a href="{{ url('/') }}/assets/javascript:void(0);">
					<h3>Oceania</h3>
					<em>(02)</em>
				</a>
			</li>
			<li>
				<a href="{{ url('/') }}/assets/javascript:void(0);">
					<h3>North America</h3>
					<em>(08)</em>
				</a>
			</li>
		</ul>
	</div>
	<!--************************************
			Search End
	*************************************-->
	<!--************************************
			Login Singup Start
	*************************************-->
	<div id="tg-loginsingup" class="tg-loginsingup" data-vide-bg="poster: {{ url('/') }}/assets/images/singup-img.jpg" data-vide-options="position: 0% 50%">
		<div class="tg-contentarea tg-themescrollbar">
			<div class="tg-scrollbar">
				<button type="button" class="close">x</button>
				<div class="tg-logincontent">
					<nav id="tg-loginnav" class="tg-loginnav">
						<ul>
							<li><a href="{{ url('/') }}/assets/#">About Us</a></li>
							<li><a href="{{ url('/') }}/assets/#">Contact Us</a></li>
							<li><a href="{{ url('/') }}/assets/#">My Account</a></li>
							<li><a href="{{ url('/') }}/assets/#">My Wishlist</a></li>
						</ul>
					</nav>
					<div class="tg-themetabs">
						<ul class="tg-navtbs" role="tablist">
							<li role="presentation" class="active"><a href="{{ url('/') }}/assets/#home" data-toggle="tab">Already Registered</a></li>
							<li role="presentation"><a href="{{ url('/') }}/assets/#profile" data-toggle="tab">New to Eventeous ?</a></li>
						</ul>
						<div class="tg-tabcontent tab-content">
							<div role="tabpanel" class="tab-pane active fade in" id="home">
								<form class="tg-formtheme tg-formlogin">
									<fieldset>
										<div class="form-group">
											<label>Name or Email <sup>*</sup></label>
											<input type="text" name="firstname" class="form-control" placeholder="">
										</div>
										<div class="form-group">
											<label>Password <sup>*</sup></label>
											<input type="password" name="password" class="form-control" placeholder="">
										</div>
										<div class="form-group">
											<div class="tg-checkbox">
												<input type="checkbox" name="remember" id="rememberpass">
												<label for="rememberpass">Remember Me</label>
											</div>
											<span><a href="{{ url('/') }}/assets/#">Lost your password?</a></span>
										</div>
										<button class="tg-btn tg-btn-lg"><span>update profile</span></button>
									</fieldset>
								</form>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="profile">
								<form class="tg-formtheme tg-formlogin">
									<fieldset>
										<div class="form-group">
											<label>Name or Email <sup>*</sup></label>
											<input type="text" name="firstname" class="form-control" placeholder="">
										</div>
										<div class="form-group">
											<label>Password <sup>*</sup></label>
											<input type="password" name="password" class="form-control" placeholder="">
										</div>
										<div class="form-group">
											<div class="tg-checkbox">
												<input type="checkbox" name="remember" id="remember">
												<label for="remember">Remember Me</label>
											</div>
											<span><a href="{{ url('/') }}/assets/#">Lost your password?</a></span>
										</div>
										<button class="tg-btn tg-btn-lg"><span>update profile</span></button>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<div class="tg-shareor"><span>or</span></div>
					<div class="tg-signupwith">
						<h2>Sign in With...</h2>
						<ul class="tg-sharesocialicon">
							<li class="tg-facebook"><a href="{{ url('/') }}/assets/#"><i class="icon-facebook-1"></i><span>Facebook</span></a></li>
							<li class="tg-twitter"><a href="{{ url('/') }}/assets/#"><i class="icon-twitter-1"></i><span>Twitter</span></a></li>
							<li class="tg-googleplus"><a href="{{ url('/') }}/assets/#"><i class="icon-google4"></i><span>Google+</span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--************************************
			Login Singup End
	*************************************-->
	<script src="{{ url('/') }}/assets/js/vendor/jquery-library.js"></script>
	<script src="{{ url('/') }}/assets/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&language=en"></script>
	<script src="{{ url('/') }}/assets/js/bootstrap-select.min.js"></script>
	<script src="{{ url('/') }}/assets/js/jquery-scrolltofixed.js"></script>
	<script src="{{ url('/') }}/assets/js/owl.carousel.min.js"></script>
	<script src="{{ url('/') }}/assets/js/jquery.mmenu.all.js"></script>
	<script src="{{ url('/') }}/assets/js/packery.pkgd.min.js"></script>
	<script src="{{ url('/') }}/assets/js/jquery.vide.min.js"></script>
	<script src="{{ url('/') }}/assets/js/scrollbar.min.js"></script>
	<script src="{{ url('/') }}/assets/js/prettyPhoto.js"></script>
	<script src="{{ url('/') }}/assets/js/countdown.js"></script>
	<script src="{{ url('/') }}/assets/js/parallax.js"></script>
	<script src="{{ url('/') }}/assets/js/gmap3.js"></script>
	<script src="{{ url('/') }}/assets/js/main.js"></script>
</body>
</html>