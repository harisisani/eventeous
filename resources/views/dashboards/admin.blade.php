<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Eventeous Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="{{ url('/') }}/assets/css/table-style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="{{ url('/') }}">Eventeous Admin Panel</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                {{-- <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
                    <button class="btn btn-primary" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button>
                </div> --}}
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><img class="rounded-circle" width="20px" src="{{ url('/') }}{{'/uploads/users/'.Auth::user()->image}}">&nbsp;<?=Auth::user()->username?></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ url('/') }}/edit-profile">User Profile</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="{{ url('/') }}/logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">View</div>
                            <a class="nav-link" href="javascript:void(0)" onclick="showSection('view-users')">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Users
                            </a>
                            <a class="nav-link" href="javascript:void(0)" onclick="showSection('view-vendors')">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Vendors
                            </a>
                            <a class="nav-link" href="javascript:void(0)" onclick="showSection('view-venue')">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Venues
                            </a>
                            <a class="nav-link" href="javascript:void(0)" onclick="showSection('view-vehicle')">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Vehicles
                            </a>
                            <a class="nav-link" href="javascript:void(0)" onclick="showSection('view-decor')">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Decors
                            </a>
                            <a class="nav-link" href="javascript:void(0)" onclick="showSection('show-payments')">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Payments
                            </a>
                            <a class="nav-link" href="javascript:void(0)" onclick="showSection('show-bookings')">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Bookings
                            </a>
                            <div class="sb-sidenav-menu-heading">Add New</div>
                            <a class="nav-link" href="javascript:void(0)" onclick="showSection('add-users')">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                User
                            </a>
                            <a class="nav-link" href="javascript:void(0)" onclick="showSection('add-vendor')">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Vendor
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        <?=Auth::user()->username?>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    {{-- view users --}}
                    <div class="container-fluid px-4 sectioncontent view-users">
                        <h1 class="mt-4">Users List</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <table id="usertable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Id</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($users as $user)
                                        <tr>
                                            <td>{{ $user->id}}</td>
                                            <td>{{ $user->username}}</td>
                                            <td>{{ $user->email}}</td>
                                            <td>{{ $user->contact}}</td>
                                            <td>{{ \Carbon\Carbon::parse($user->created_at)->format('d/m/Y g:i:s')}}</td>
                                            <td>{{ \Carbon\Carbon::parse($user->updated_at)->format('d/m/Y g:i:s')}}</td>
                                            <td>
                                            <ul class="list-inline m-0">
                                                {{-- <li  class="list-inline-item">
                                                    <button  class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                                </li> --}}
                                                <li class="list-inline-item">
                                                    <button onclick="deleteUser('{{ $user->id}}',this)" class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
                                                </li>
                                            </ul>
                                        </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- view vendors --}}
                    <div style="display:none;" class="container-fluid px-4 sectioncontent view-vendors">
                        <h1 class="mt-4">Vendors List</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Vendors</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <table id="vendortable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Id</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($vendors as $vendor)
                                        <tr>
                                            <td>{{ $vendor->id}}</td>
                                            <td>{{ $vendor->username}}</td>
                                            <td>{{ $vendor->email}}</td>
                                            <td>{{ $vendor->contact}}</td>
                                            <td>{{ \Carbon\Carbon::parse($vendor->created_at)->format('d/m/Y g:i:s')}}</td>
                                            <td>{{ \Carbon\Carbon::parse($vendor->updated_at)->format('d/m/Y g:i:s')}}</td>
                                            <td>
                                            <ul class="list-inline m-0">
                                                {{-- <li  class="list-inline-item">
                                                    <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                                </li> --}}
                                                <li class="list-inline-item">
                                                    <button onclick="deleteUser('{{ $vendor->id}}',this)" class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
                                                </li>
                                            </ul>
                                        </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- view venues --}}
                    <div style="display:none;" class="container-fluid px-4 sectioncontent view-venue">
                        <h1 class="mt-4">All Venues</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Venues</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <table id="venuetable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Short Description</th>
                                            <th>Vendor Id</th>
                                            <th>Price/Frequency</th>
                                            <th>City</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Short Description</th>
                                            <th>Vendor Id</th>
                                            <th>Price/Frequency</th>
                                            <th>City</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($venues as $venue)
                                        <tr>
                                            <?php
                                                $url=url('/venue/'.$venue->id);
                                                $service_name='<a href="'.$url.'">'.$venue->name.'</a>';
                                            ?>
                                            <td>{{ $venue->id}}</td>
                                            <td><?=$service_name?></td>
                                            <td>
                                                <button type="button" onclick="seeDetails('<?=$venue->id?>',this)" class="btn btn-info">Details</button>
                                                <input type="hidden" class="description" value="{{ $venue->description}}">
                                            </td>
                                            <td>{{ $venue->notes}}</td>
                                            <td>{{ $venue->vendors_id}}</td>
                                            <td>{{ $venue->price}}</td>
                                            <td>{{ $venue->city}}</td>
                                            <td>{{ \Carbon\Carbon::parse($venue->created_at)->format('d/m/Y g:i:s')}}</td>
                                            <td>{{ \Carbon\Carbon::parse($venue->updated_at)->format('d/m/Y g:i:s')}}</td>
                                            <td style="width:10%">
                                            <ul class="list-inline m-0">
                                                {{-- <li class="list-inline-item">
                                                    <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                                </li> --}}
                                                <li class="list-inline-item">
                                                    <button  onclick="deleteItem('{{ $venue->name}}','{{ $venue->id}}',this,'venue')" class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
                                                </li>
                                            </ul>
                                        </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- view vehicles --}}
                    <div style="display:none;" class="container-fluid px-4 sectioncontent view-vehicle">
                        <h1 class="mt-4">Vendors List</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Vendors</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <table id="vehicletable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Short Description</th>
                                            <th>Vendor Id</th>
                                            <th>Price/Frequency</th>
                                            <th>City</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Short Description</th>
                                            <th>Vendor Id</th>
                                            <th>Price/Frequency</th>
                                            <th>City</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($vehicles as $vehicle)
                                        <tr>
                                            <?php
                                                $url=url('/vehicle/'.$vehicle->id);
                                                $service_name='<a href="'.$url.'">'.$vehicle->name.'</a>';
                                            ?>
                                            <td>{{ $vehicle->id}}</td>
                                            <td><?=$service_name?></td>
                                            <td>
                                                <button type="button" onclick="seeDetails('<?=$vehicle->id?>',this)" class="btn btn-info">Details</button>
                                                <input type="hidden" class="description" value="{{ $vehicle->description}}">
                                            </td>
                                            <td>{{ $vehicle->notes}}</td>
                                            <td>{{ $vehicle->vendors_id}}</td>
                                            <td>{{ $vehicle->price}}</td>
                                            <td>{{ $vehicle->city}}</td>
                                            <td>{{ \Carbon\Carbon::parse($vehicle->created_at)->format('d/m/Y g:i:s')}}</td>
                                            <td>{{ \Carbon\Carbon::parse($vehicle->updated_at)->format('d/m/Y g:i:s')}}</td>
                                            <td style="width:10%">
                                            <ul class="list-inline m-0">
                                                {{-- <li class="list-inline-item">
                                                    <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                                </li> --}}
                                                <li class="list-inline-item">
                                                    <button onclick="deleteItem('{{ $venue->name}}','{{ $venue->id}}',this,'vehicle')" class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
                                                </li>
                                            </ul>
                                        </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- view decors --}}
                    <div style="display:none;" class="container-fluid px-4 sectioncontent view-decor">
                        <h1 class="mt-4">Vendors List</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Vendors</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <table id="decortable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Short Description</th>
                                            <th>Vendor Id</th>
                                            <th>Price/Frequency</th>
                                            <th>City</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Short Description</th>
                                            <th>Vendor Id</th>
                                            <th>Price/Frequency</th>
                                            <th>City</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($decors as $decor)
                                        <tr>
                                            <?php
                                                $url=url('/decor/'.$decor->id);
                                                $service_name='<a href="'.$url.'">'.$decor->name.'</a>';
                                            ?>
                                            <td>{{ $decor->id}}</td>
                                            <td><?=$service_name?></td>
                                            <td>
                                                <button type="button" onclick="seeDetails('<?=$decor->id?>',this)" class="btn btn-info">Details</button>
                                                <input type="hidden" class="description" value="{{ $decor->description}}">
                                            </td>
                                            <td>{{ $decor->notes}}</td>
                                            <td>{{ $decor->vendors_id}}</td>
                                            <td>{{ $decor->price}}</td>
                                            <td>{{ $decor->city}}</td>
                                            <td>{{ \Carbon\Carbon::parse($decor->created_at)->format('d/m/Y g:i:s')}}</td>
                                            <td>{{ \Carbon\Carbon::parse($decor->updated_at)->format('d/m/Y g:i:s')}}</td>
                                            <td style="width:10%">
                                            <ul class="list-inline m-0">
                                                {{-- <li class="list-inline-item">
                                                    <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                                </li> --}}
                                                <li class="list-inline-item">
                                                    <button onclick="deleteItem('{{ $venue->name}}','{{ $venue->id}}',this,'decor')"  class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
                                                </li>
                                            </ul>
                                        </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- add users --}}
                    <div style="display:none;" class="container sectioncontent add-users">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Create Account</h3></div>
                                    <div class="card-body">
                                        <form>
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3 mb-md-0">
                                                        <input class="form-control" id="inputFirstName" name="username" type="text" placeholder="Enter your full name" />
                                                        <label for="inputFirstName">Username</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating">
                                                        <input class="form-control" id="inputLastName"  name="contact" type="text" placeholder="Enter your contact" />
                                                        <label for="inputLastName">Contact</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-floating mb-3">
                                                <input class="form-control" id="inputEmail" name="email" type="email" placeholder="Enter your email" />
                                                <label for="inputEmail">Email address</label>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3 mb-md-0">
                                                        <input class="form-control" id="inputPassword" name="password" type="password" placeholder="Enter a password" />
                                                        <label for="inputPassword">Password</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3 mb-md-0">
                                                    <select class="form-control" style="width:100%;" name="usertype" id="">
                                                        <option selected value="user">User</option>
                                                    </select>
                                                    <label for="inputPassword">User Type</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-4 mb-0">
                                                <div class="d-grid"><a class="btn btn-primary btn-block btn-submit-user" href="javascript:void(0)">Create Account</a></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- add vendors --}}
                    <div style="display:none;" class="container sectioncontent add-vendor">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Create Account</h3></div>
                                    <div class="card-body">
                                        <form>
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3 mb-md-0">
                                                        <input class="form-control" id="inputFirstName" name="usernamevendor" type="text" placeholder="Enter your full name" />
                                                        <label for="inputFirstName">Username</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating">
                                                        <input class="form-control" id="inputLastName"  name="contactvendor" type="text" placeholder="Enter your contact" />
                                                        <label for="inputLastName">Contact</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-floating mb-3">
                                                <input class="form-control" id="inputEmail" name="emailvendor" type="email" placeholder="Enter your email" />
                                                <label for="inputEmail">Email address</label>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3 mb-md-0">
                                                        <input class="form-control" id="inputPassword" name="passwordvendor" type="password" placeholder="Enter a password" />
                                                        <label for="inputPassword">Password</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3 mb-md-0">
                                                    <select class="form-control" style="width:100%;" name="usertypevendor" id="">
                                                        <option selected value="vendor">Vendor</option>
                                                    </select>
                                                    <label for="inputPassword">User Type</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-4 mb-0">
                                                <div class="d-grid"><a class="btn btn-primary btn-block btn-submit-vendor" href="javascript:void(0)">Create Account</a></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                     <!-- show payments -->
                    <div style="display:none;" class="container sectioncontent show-payments">
                        <h1 class="mt-4">Payment List</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Payments</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <table id="paymenttable">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Name on Card</th>
                                            <th>Card Number</th>
                                            <th>Method</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>User</th>
                                            <th>Name on Card</th>
                                            <th>Card Number</th>
                                            <th>Method</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach ($payments as $pay )
                                            <tr>
                                                <?php
                                                $array = json_decode(json_encode($usersWithDeleted), true);
                                                foreach($array as $user){
                                                    if($user['id']==$pay->user_id){
                                                        $user_name=$user['username'];
                                                    }
                                                }?>
                                                <td><a href="{{ url('/view-profile/') }}<?='/'.$pay->user_id?>"><?=$user_name?></a></td>
                                                <td>{{ $pay->name_on_card}}</td>
                                                <td>XXXX-XXXX-XXXX-{{ substr($pay->card_number,12,16)}}</td>
                                                <td>Visa</td>
                                                <td>{{ $pay->amount}}</td>
                                                <td>Paid</td>
                                                <td>{{ \Carbon\Carbon::parse($pay->created_at)->format('d/m/Y g:i:s')}}</td>
                                                <td>{{ \Carbon\Carbon::parse($pay->updated_at)->format('d/m/Y g:i:s')}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- show bookings -->
                    <div style="display:none;" class="container sectioncontent show-bookings">
                        <h1 class="mt-4">Booking List</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Bookings</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                @if (isset($bookings) > 0)
                                <table id="bookingTable">
                                    <thead>
                                        <tr>
                                            <th>Customer</th>
                                            <th>Vendor</th>
                                            <th>Name</th>
                                            <th>Booking Date</th>
                                            <th>Status</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Customer</th>
                                            <th>Vendor</th>
                                            <th>Name</th>
                                            <th>Booking Date</th>
                                            <th>Status</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                            @foreach($bookings as $value)
                                            <?php
                                            $array = json_decode(json_encode($usersWithDeleted), true);
                                                foreach($array as $user){
                                                    if($user['id']==$value->user_id){
                                                    $customer=$user['username'];
                                                }
                                                if($user['id']==$value->vendor_id){
                                                    $vendor=$user['username'];
                                                }
                                                }
                                            if($value->booking_category=="venue"){
                                                $array = json_decode(json_encode($venuesWithDeleted), true);
                                                    foreach($array as $valueArray){
                                                        if($valueArray['id']==$value->booked_id){
                                                            if($valueArray['deleted']==0){
                                                                $url=url('/venue/'.$value->booked_id);
                                                                $service_name='<a href="'.$url.'">'.$valueArray['name'].'</a>';
                                                            }else{
                                                                $service_name=$valueArray['name'];
                                                            }

                                                        }
                                                    }
                                            }
                                            if($value->booking_category=="vehicle"){
                                            $array = json_decode(json_encode($vehiclesWithDeleted), true);
                                                foreach($array as $valueArray){
                                                    if($valueArray['id']==$value->booked_id){
                                                        if($valueArray['deleted']==0){
                                                        $url=url('/vehicles/'.$value->booked_id);
                                                        $service_name=$valueArray['name'];
                                                        $service_name='<a href="'.$url.'">'.$valueArray['name'].'</a>';
                                                        }else{
                                                            $service_name=$valueArray['name'];
                                                        }
                                                    }
                                                }
                                            }
                                            if($value->booking_category=="decor"){
                                            $array = json_decode(json_encode($decorsWithDeleted), true);
                                                foreach($array as $valueArray){
                                                    if($valueArray['id']==$value->booked_id){
                                                        if($valueArray['deleted']==0){
                                                        $url=url('/decors/'.$value->booked_id);
                                                        $service_name=$valueArray['name'];
                                                        $service_name='<a href="'.$url.'">'.$valueArray['name'].'</a>';
                                                        }else{
                                                            $service_name=$valueArray['name'];
                                                        }
                                                    }
                                                }
                                            }
                                            if($value->approvalStatus=="Accepted"){
                                                $status='<span class="badge badge-success">Accepted</span>';
                                            }else if($value->approvalStatus=="user_cancel"){
                                                $status='<span class="badge badge-danger">'.$customer.' Cancelled</span>';
                                            }else if($value->approvalStatus=="vendor_cancel"){
                                                $status='<span class="badge badge-danger">'.$vendor.' Cancelled</span>';
                                            }else{
                                                $status='<span class="badge badge-warning">Pending</span>';
                                            }
                                            ?>
                                            <tr>
                                                <td><a href="{{ url('/view-profile/') }}<?='/'.$value->user_id?>"><?=$customer?></a></td>
                                                <td><a href="{{ url('/view-profile/') }}<?='/'.$value->user_id?>"><?=$vendor?></a></td>
                                                <td><?=$service_name?></td>
                                                <td><?=$value->booking_date?></td>
                                                <td class="status"><?=$status?></td>
                                                <td><?=$value->created_at?></td>
                                                <td><?=$value->updated_at?></td>
                                                <td>
                                                    <button type="button" onclick="seeDetails('<?=$value->id?>',this)" class="btn btn-info">Details</button>
                                                    <input type="hidden" class="description" value="<?=$value->bookingDescription?>">
                                                </td>
                                            </tr>
                                            @endforeach
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Eventeous 2022</div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="{{ url('/') }}/assets/js/table-script.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="{{ url('/') }}/assets/js/table-simple-demo.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <script src="{{ url('/') }}/assets/login-register/sweetalert2@11.js"></script>
    </body>
</html>
<script>
    function showSection(indexClass){
        $('.sectioncontent').hide();
        $('.'+indexClass).show();
    }
</script>
<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(".btn-submit-vendor").click(function(e){
        e.preventDefault();
        var email = $("input[name=emailvendor]").val();
        var username = $("input[name=usernamevendor]").val();
        var password = $("input[name=passwordvendor]").val();
		if(email=="" || username=="" || password=="" || contact==""){
			Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Fields with * are required'
			})
		}else if(password.length<6){
			Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: 'Password should be atleast 6 characters'
			})
		}else{
			var contact = $("input[name=contactvendor]").val();
			var usertype = $("select[name=usertypevendor]").val();
			$.ajax({
			   type:'POST',
			   url:"{{ route('register.post') }}",
			   data:{username:username, password:password, email:email, contact:contact, usertype:usertype},
			   success:function(data){
				Swal.fire({
						icon: 'success',
						title: 'Good job!',
						text: 'Account created successfully'
					})
			   },
			   error: function (error) {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Email already taken'
					})
				}
			});
		}
    });
    $(".btn-submit-user").click(function(e){
        e.preventDefault();
        var email = $("input[name=email]").val();
        var username = $("input[name=username]").val();
        var password = $("input[name=password]").val();
		if(email=="" || username=="" || password=="" || contact==""){
			Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Fields with * are required'
			})
		}else if(password.length<6){
			Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: 'Password should be atleast 6 characters'
			})
		}else{
			var contact = $("input[name=contact]").val();
			var usertype = $("select[name=usertype]").val();
			$.ajax({
			   type:'POST',
			   url:"{{ route('register.post') }}",
			   data:{username:username, password:password, email:email, contact:contact, usertype:usertype},
			   success:function(data){
				Swal.fire({
						icon: 'success',
						title: 'Good job!',
						text: 'Account created successfully'
					})
			   },
			   error: function (error) {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Email already taken'
					})
				}
			});
		}
    });

    function deleteUser(id,obj){
        Swal.fire({
            title: 'Are you sure to delete user/vendor with id: '+id,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.isConfirmed) {
                $(obj).closest('tr').remove();
                $.ajax({
			   type:'POST',
			   url:"{{ route('delete.user') }}",
               data:{id:id},
			   success:function(data){
				Swal.fire({
						icon: 'success',
						title: 'Good job!',
						text: 'Account deleted successfully'
					});
                var tr=$(obj).closest('tr');
			   },
			   error: function (error) {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Server Error'
					})
				}
			});
            }
        })
    }


    function seeDetails(id,obj){
        var tr=(obj).closest('tr');
        var text=$(tr).find('input.description').val();
        $('#desc').html(text);
        $('#seeDetails').show();
    }

    function closeDetails(){
        $('#seeDetails').hide();
    }

    function deleteItem(name,id,obj,category){
        if(category=="venue"){
            var url = "{{ route('delete.venue') }}";
        }
        if(category=="vehicle"){
            var url = "{{ route('delete.vehicle') }}";
        }
        if(category=="decor"){
            var url = "{{ route('delete.decor') }}";
        }
        Swal.fire({
            title: 'Are you sure to delete name: '+name,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.isConfirmed) {
                $(obj).closest('tr').remove();
                $.ajax({
			   type:'POST',
			   url:url,
               data:{id:id},
			   success:function(data){
				Swal.fire({
						icon: 'success',
						title: 'Good job!',
						text: 'Items deleted successfully'
					});
                var tr=$(obj).closest('tr');
			   },
			   error: function (error) {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Server Error'
					})
				}
			});
            }
        })
}
</script>
<div class="modal" id="seeDetails" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" onclick="closeDetails()" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="desc" class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" onclick="closeDetails()" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
