<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="zxx">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Eventeous</title>
    <meta name="description" content="">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{ url('/') }}/assets/apple-touch-icon.png">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/normalize.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/icomoon.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap-select.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/scrollbar.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/jquery.mmenu.all.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/prettyPhoto.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/transitions.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/color.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/responsive.css">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script src="{{ url('/') }}/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body style="background: #FF7550" class="tg-home tg-homevone">
    @include('header')
<div style="margin-top:100px !important;padding-left:25%;padding-right:25%;max-width:80%; min-width:50%" class="container rounded bg-white mt-5 mb-5">
    @if(isset($users))
    @foreach ($users as $user)
    <div class="row border-right border-left">
        <div class="col-12">
            <div class="d-flex flex-column align-items-center text-center p-3 py-5"><img id="imageLink" class="rounded-circle mt-5" width="150px" src="{{ url('/') }}{{'/uploads/users/'.$user->image}}"><span class="font-weight-bold">{{ $user->username }}</span></span class="text-black-50">{{ $user->email }}</span><span> </span></div>
        </div>
        <form id="saveProfile" method="POST" action="{{ route('save.profile') }}">
            {{-- @csrf --}}
            <div class="col-12">
                <div class="p-3 py-5">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h4 class="text-right">Profile Settings</h4>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <label class="labels">Username</label>
                            <input type="text" class="form-control" name="username" placeholder="enter username" value="<?=$user->username?>">
                            <input type="hidden" class="form-control" name="id" placeholder="enter username" value="<?=$user->id?>">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12"><label class="labels">Contact</label><input name="contact" type="text" class="form-control" placeholder="enter contact" value="<?=$user->contact?>"></div>
                        <div class="col-md-12"><label class="labels">New Password</label><input name="password" type="password" class="form-control" placeholder="enter password" value=""></div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-12"><label class="labels">Profile Photo</label><input type="file" id="image" name="image" class="form-control" placeholder="profile picture" value=""></div>
                        <div class="mt-5 text-right"><but onclick="saveProfileUser();" class="btn btn-primary profile-button" type="button">Save Profile</but></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @endforeach
    @endif
</div>

<style>
    body {
        background: rgb(99, 39, 120)
    }

    .form-control:focus {
        box-shadow: none;
        border-color: #BA68C8
    }

    .profile-button {
        background: rgb(99, 39, 120);
        box-shadow: none;
        border: none
    }

    .profile-button:hover {
        background: #682773
    }

    .profile-button:focus {
        background: #682773;
        box-shadow: none
    }

    .profile-button:active {
        background: #682773;
        box-shadow: none
    }

    .back:hover {
        color: #682773;
        cursor: pointer
    }

    .labels {
        font-size: 11px
    }

    .add-experience:hover {
        background: #BA68C8;
        color: #fff;
        cursor: pointer;
        border: solid 1px #BA68C8
    }
</style>

    <!--************************************
			Login Singup End
	*************************************-->
    <script src="{{ url('/') }}/assets/js/vendor/jquery-library.js"></script>
    <script src="{{ url('/') }}/assets/js/vendor/bootstrap.min.js"></script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&language=en"></script>
    <script src="{{ url('/') }}/assets/js/bootstrap-select.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery-scrolltofixed.js"></script>
    <script src="{{ url('/') }}/assets/js/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.mmenu.all.js"></script>
    <script src="{{ url('/') }}/assets/js/packery.pkgd.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.vide.min.js"></script>
    <script src="{{ url('/') }}/assets/js/scrollbar.min.js"></script>
    <script src="{{ url('/') }}/assets/js/prettyPhoto.js"></script>
    <script src="{{ url('/') }}/assets/js/countdown.js"></script>
    <script src="{{ url('/') }}/assets/js/parallax.js"></script>
    <script src="{{ url('/') }}/assets/js/gmap3.js"></script>
    <script src="{{ url('/') }}/assets/js/main.js"></script>
    <script src="{{ url('/') }}/assets/login-register/sweetalert2@11.js"></script>
</body>

</html>
<script>
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function saveProfileUser(){
        let formData = new FormData($('#saveProfile')[0]);
			$.ajax({
                type: 'post',
                url:"{{ url('save-profile') }}",
                data:formData,
                contentType:false,
                processData:false,
                success:function(data){
                    location.reload();
			   },
			   error: function (error) {
					// Swal.fire({
					// 	icon: 'error',
					// 	title: 'Oops...',
					// 	text: 'Server Error'
					// })
                    location.reload();
				}
			});

}
</script>
