<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Eventeous User</title>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="{{ url('/') }}/assets/css/table-style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <!-- Navbar Brand-->
        <a class="navbar-brand ps-3" href="{{ url('/') }}">Eventeous User Panel</a>
        <!-- Sidebar Toggle-->
        <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
        <!-- Navbar Search-->
        <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
            {{-- <div class="input-group">
                <input class="form-control" type="text" placeholder="Search for..." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
                <button class="btn btn-primary" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button>
            </div> --}}
        </form>
        <!-- Navbar-->
        <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><img class="rounded-circle" width="20px" src="{{ url('/') }}{{'/uploads/users/'.Auth::user()->image}}">&nbsp;<?=Auth::user()->username?></a>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="{{ url('/') }}/edit-profile">User Profile</a></li>
                    <li>
                        <hr class="dropdown-divider" />
                    </li>
                    <li><a class="dropdown-item" href="{{ url('/') }}/logout">Logout</a></li>
                </ul>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="sb-sidenav-menu-heading">View</div>
                        <a class="nav-link" href="javascript:void(0)" onclick="showSection('show-payments')">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            Payment Details
                        </a>
                        <a class="nav-link" href="javascript:void(0)" onclick="showSection('show-bookings')">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            My Bookings
                        </a>

                    </div>
                </div>
                <div class="sb-sidenav-footer">
                    <div class="small">Logged in as:</div>
                    <?=Auth::user()->username?>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                 <!-- show payments -->
                 <div class="container sectioncontent show-payments">
                    <h1 class="mt-4">Payment List</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active">Payments</li>
                    </ol>
                    <div class="card mb-4">
                        <div class="card-body">
                            <table id="paymenttable">
                                <thead>
                                    <tr>
                                        <th>Name on Card</th>
                                        <th>Card Number</th>
                                        <th>Method</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Created at</th>
                                        <th>Updated at</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Name on Card</th>
                                        <th>Card Number</th>
                                        <th>Method</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Created at</th>
                                        <th>Updated at</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach ($payments as $pay )
                                        <tr>
                                            <td>{{ $pay->name_on_card}}</td>
                                            <td>XXXX-XXXX-XXXX-{{ substr($pay->card_number,12,16)}}</td>
                                            <td>Visa</td>
                                            <td>{{ $pay->amount}}</td>
                                            <td>Paid</td>
                                            <td>{{ \Carbon\Carbon::parse($pay->created_at)->format('d/m/Y g:i:s')}}</td>
                                            <td>{{ \Carbon\Carbon::parse($pay->updated_at)->format('d/m/Y g:i:s')}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                 <!-- show bookings -->
                 <div style="display:none;" class="container sectioncontent show-bookings">
                    <h1 class="mt-4">Booking List</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active">Bookings</li>
                    </ol>
                    <div class="card mb-4">
                        <div class="card-body">
                            @if (isset($bookings) > 0)
                            <table id="bookingTable">
                                <thead>
                                    <tr>
                                        <th>Customer</th>
                                        <th>Name</th>
                                        <th>Booking Date</th>
                                        <th>Status</th>
                                        <th>Created at</th>
                                        <th>Updated at</th>
                                        <th colspan=3>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Customer</th>
                                        <th>Name</th>
                                        <th>Booking Date</th>
                                        <th>Status</th>
                                        <th>Created at</th>
                                        <th>Updated at</th>
                                        <th colspan=3>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                        @foreach($bookings as $value)
                                        <?php
                                        $array = json_decode(json_encode($usersWithDeleted), true);
                                            foreach($array as $user){
                                                if($user['id']==$value->vendor_id){
                                                    $user_name=$user['username'];
                                                }
                                            }
                                        if($value->booking_category=="venue"){
                                            $array = json_decode(json_encode($venuesWithDeleted), true);
                                                foreach($array as $valueArray){
                                                    if($valueArray['id']==$value->booked_id){
                                                        if($valueArray['deleted']==0){
                                                            $url=url('/venue/'.$value->booked_id);
                                                            $service_name='<a href="'.$url.'">'.$valueArray['name'].'</a>';
                                                        }else{
                                                            $service_name=$valueArray['name'];
                                                        }

                                                    }
                                                }
                                        }
                                        if($value->booking_category=="vehicle"){
                                        $array = json_decode(json_encode($vehiclesWithDeleted), true);
                                            foreach($array as $valueArray){
                                                if($valueArray['id']==$value->booked_id){
                                                    if($valueArray['deleted']==0){
                                                    $url=url('/vehicles/'.$value->booked_id);
                                                    $service_name=$valueArray['name'];
                                                    $service_name='<a href="'.$url.'">'.$valueArray['name'].'</a>';
                                                    }else{
                                                        $service_name=$valueArray['name'];
                                                    }
                                                }
                                            }
                                        }
                                        if($value->booking_category=="decor"){
                                        $array = json_decode(json_encode($decorsWithDeleted), true);
                                            foreach($array as $valueArray){
                                                if($valueArray['id']==$value->booked_id){
                                                    if($valueArray['deleted']==0){
                                                    $url=url('/decors/'.$value->booked_id);
                                                    $service_name=$valueArray['name'];
                                                    $service_name='<a href="'.$url.'">'.$valueArray['name'].'</a>';
                                                    }else{
                                                        $service_name=$valueArray['name'];
                                                    }
                                                }
                                            }
                                        }
                                        if($value->approvalStatus=="Accepted"){
                                            $status='<span class="badge badge-success">Accepted</span>';
                                        }else if($value->approvalStatus=="user_cancel"){
                                            $status='<span class="badge badge-danger">You Cancelled</span>';
                                        }else if($value->approvalStatus=="vendor_cancel"){
                                            $status='<span class="badge badge-danger">'.$user_name.' Cancelled</span>';
                                        }else{
                                            $status='<span class="badge badge-warning">Pending</span>';
                                        }
                                        ?>
                                        <tr>
                                            <td><a href="{{ url('/view-profile/') }}<?='/'.$value->vendor_id?>"><?=$user_name?></a></td>
                                            <td><?=$service_name?></td>
                                            <td><?=$value->booking_date?></td>
                                            <td class="status"><?=$status?></td>
                                            <td><?=$value->created_at?></td>
                                            <td><?=$value->updated_at?></td>
                                            <td>
                                                <button type="button" onclick="seeDetails('<?=$value->id?>',this)" class="btn btn-info">Details</button>
                                                <input type="hidden" class="description" value="<?=$value->bookingDescription?>">
                                            </td>
                                            <td>
                                                <button type="button" onclick="bookingCancel('<?=$user_name?>','<?=$value->id?>',this)" class="btn btn-danger">Cancel</button>
                                            </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                </div>
            </main>

        </div>

        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Eventeous 2022</div>
                </div>
            </div>
        </footer>

    </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="{{ url('/') }}/assets/js/table-script.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="{{ url('/') }}/assets/js/table-simple-demo.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
	<script src="{{ url('/') }}/assets/login-register/sweetalert2@11.js"></script>
</body>

</html>
<script>
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function showSection(indexClass) {
        $('.sectioncontent').hide();
        $('.' + indexClass).show();
    }

    function seeDetails(id,obj){
        var tr=(obj).closest('tr');
        var text=$(tr).find('input.description').val();
        $('#desc').html(text);
        $('#seeDetails').show();
    }

    function closeDetails(){
        $('#seeDetails').hide();
    }

    function bookingCancel(user,id,obj){
        Swal.fire({
            title: 'Are you sure cancel booking from: '+user,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, cancel it!'
            }).then((result) => {
            if (result.isConfirmed) {
                var tr=(obj).closest('tr');
                $(tr).find('td.status').html('<span class="badge badge-danger">You Cancelled</span>');
                $.ajax({
            type:'POST',
            url:"{{ route('booking.update') }}",
            data:{id:id,status:"user_cancel"},
            success:function(data){
                Swal.fire({
                        icon: 'success',
                        title: 'Good job!',
                        text: 'Booking cancelled successfully'
                    });
            },
            error: function (error) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Server Error'
                    })
                }
            });
            }
        })
    }
</script>
<div class="modal" id="seeDetails" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" onclick="closeDetails()" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="desc" class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" onclick="closeDetails()" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
