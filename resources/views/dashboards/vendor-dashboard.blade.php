<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Eventeous Vendor</title>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="{{ url('/') }}/assets/css/table-style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{-- calendar --}}
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500&display=swap" rel="stylesheet">


    <link rel="stylesheet" href="{{ url('/') }}/assets/calendar/fonts/icomoon/style.css">

    <link href='{{ url('/') }}/assets/calendar/fullcalendar/packages/core/main.css' rel='stylesheet' />
    <link href='{{ url('/') }}/assets/calendar/fullcalendar/packages/daygrid/main.css' rel='stylesheet' />


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/calendar/css/bootstrap.min.css">

    <!-- Style -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/calendar/css/style.css">
    {{-- calendar --}}
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <!-- Navbar Brand-->
        <a class="navbar-brand ps-3" href="{{ url('/') }}">Eventeous Vendor Panel</a>
        <!-- Sidebar Toggle-->
        <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
        <!-- Navbar Search-->
        <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
            {{-- <div class="input-group">
                <input class="form-control" type="text" placeholder="Search for..." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
                <button class="btn btn-primary" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button>
            </div> --}}
        </form>
        <!-- Navbar-->
        <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><img class="rounded-circle" width="20px" src="{{ url('/') }}{{'/uploads/users/'.Auth::user()->image}}">&nbsp;<?=Auth::user()->username?></a>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="{{ url('/edit-profile/') }}">User Profile</a></li>
                    <li>
                        <hr class="dropdown-divider" />
                    </li>
                    <li><a class="dropdown-item" href="{{ url('/') }}/logout">Logout</a></li>
                </ul>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="sb-sidenav-menu-heading">View</div>
                        <a class="nav-link" href="javascript:void(0)" onclick="showSection('view-venues')">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            My venues
                        </a>
                        <a class="nav-link" href="javascript:void(0)" onclick="showSection('view-vehicles')">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            My Vehicles
                        </a>
                        <a class="nav-link" href="javascript:void(0)" onclick="showSection('view-decors')">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            My Decors
                        </a>
                        <a class="nav-link" href="javascript:void(0)" onclick="showSection('show-payments')">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            Payment Details
                        </a>
                        <a class="nav-link" href="javascript:void(0)" onclick="showSection('show-bookings')">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            Bookings
                        </a>
                        <a class="nav-link" href="javascript:void(0)" onclick="showSection('show-tentative')">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            Tentative
                        </a>
                        <div class="sb-sidenav-menu-heading">Add New</div>
                        <a class="nav-link" href="javascript:void(0)" onclick="showSection('add-venue')">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            Add Venue
                        </a>
                        <a class="nav-link" href="javascript:void(0)" onclick="showSection('add-vehicle')">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            Add Vehicle
                        </a>
                        <a class="nav-link" href="javascript:void(0)" onclick="showSection('add-decor')">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            Add Decors
                        </a>

                    </div>
                </div>
                <div class="sb-sidenav-footer">
                    <div class="small">Logged in as:</div>
                    <?=Auth::user()->username?>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid px-4 sectioncontent view-venues">
                    <h1 class="mt-4">All Venues</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Venues</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <table id="venuetable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Short Description</th>
                                            <th>Price/Frequency</th>
                                            <th>City</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Short Description</th>
                                            <th>Price/Frequency</th>
                                            <th>City</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($venues as $venue)
                                        <tr>
                                            <?php
                                                $url=url('/venue/'.$venue->id);
                                                $service_name='<a href="'.$url.'">'.$venue->name.'</a>';
                                            ?>
                                            <td>{{ $venue->id}}</td>
                                            <td><?=$service_name?></td>
                                            <td>
                                                <button type="button" onclick="seeDetails('<?=$venue->id?>',this)" class="btn btn-info">Details</button>
                                                <input type="hidden" class="description" value="{{ $venue->description}}">
                                            </td>
                                            <td>{{ $venue->notes}}</td>
                                            <td>{{ $venue->price}}</td>
                                            <td>{{ $venue->city}}</td>
                                            <td>{{ \Carbon\Carbon::parse($venue->created_at)->format('d/m/Y g:i:s')}}</td>
                                            <td>{{ \Carbon\Carbon::parse($venue->updated_at)->format('d/m/Y g:i:s')}}</td>
                                            <td style="width:10%">
                                            <ul class="list-inline m-0">
                                                {{-- <li class="list-inline-item">
                                                    <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                                </li> --}}
                                                <li class="list-inline-item">
                                                    <button class="btn btn-danger btn-sm rounded-0" onclick="deleteItem('{{ $venue->name}}','{{ $venue->id}}',this,'venue')" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
                                                </li>
                                            </ul>
                                        </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>


            <!-- add venuew -->
                <div style="display:none;" class="container sectioncontent add-venue">
                    <div class="row justify-content-center">
                        <div class="col-lg-7">
                            <?php if(count($subs)>0 && $subs[0]['available_connects']>0){?>
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header">
                                        <h3 class="text-center font-weight-light my-4">Add New Venue</h3>
                                        <p>Booking Connects Available: <?=$subs[0]['available_connects']?> | <a target="_blank" href="{{  url('/packages/') }}">Get More Connects</a></p>
                                    </div>
                                    <div class="card-body">
                                        <form id="addVenue" enctype="multipart/form-data">
                                                @csrf
                                                    <div class="row mb-3">
                                                        <div class="col-md-12">
                                                            <div class="form-floating">
                                                                <input class="form-control" id="inputLastName" type="text" name="venueName" placeholder="Enter Name of the venue" />
                                                                <label for="inputLastName">Venue Name*</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-floating mb-3 mb-md-0">
                                                                <select class="form-control" style="width:100%;" name="venueCity" id="">
                                                                    <option selected value="Karachi">Karachi</option>
                                                                    <option value="Lahore">Lahore</option>
                                                                    <option value="Islamabad-Rawalpindi">Islamabad-Rawalpindi</option>
                                                                    <option value="Peshawar">Peshawar</option>
                                                                    <option value="Faisalabad">Faisalabad</option>
                                                                    <option value="Multan">Multan</option>
                                                                    <option value="Gujranwala">Gujranwala</option>
                                                                    <option value="Sialkot">Sialkot</option>
                                                                </select>
                                                                <label for="inputPassword">Select City</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-floating">
                                                                <input class="form-control" id="inputLastName" name="venuePrice" maxlength="8" type="text" placeholder="Enter Area of the venue" />
                                                                <label for="inputLastName">Price/Frequency</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-floating mb-3">
                                                                <label for="details">Complete Details</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3 mt-5" >
                                                        <div class="col-md-12">
                                                            <div class="form-floating mb-3">
                                                                <textarea class="form-control" name="venueDescription" id="venueDescription" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3 mt-5" >
                                                        <div class="col-md-12">
                                                            <div class="form-floating mb-3">
                                                                <textarea class="form-control" name="venueNotes" id="venueNotes" cols="30" rows="10"></textarea>
                                                                <label for="details">Short Notes</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-floating mb-3">
                                                                <input class="form-control" id="venueImage" name="venueImage" type="file" />
                                                                <label for="inputEmail">Upload Image</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mt-4 mb-3">
                                                <div class="d-grid">
                                                    <button type="button" class="save-venue btn btn-primary btn-block">Add Venue</button>
                                                </div>
                                                </div>
                                        </form>
                                    </div>
                                    <?php }?>
                                    <?php if(count($subs)>0 && $subs[0]['available_connects']<=0){?>
                                        <div class="card-header">
                                            <h3 class="text-center font-weight-light my-4">Add New Venue</h3>
                                        </div>
                                        <?php } if(count($subs)<=0){?>
                                        <div class="card-header">
                                            <h3 class="text-center font-weight-light my-4">Add New Venue</h3>
                                            <p>No Bookings Connects Available | <a target="_blank" href="{{  url('/packages/') }}">Get Connects to add Venues</a></p>
                                        </div>
                                    <?php }?>
                            </div>
                        </div>
                    </div>

                <!-- add vehicles -->
                <div style="display:none;" class="container sectioncontent add-vehicle">
                    <div class="row justify-content-center">
                        <div class="col-lg-7">
                            <?php if(count($subs)>0 && $subs[0]['available_connects']>0){?>
                            <div class="card shadow-lg border-0 rounded-lg mt-5">
                                <div class="card-header">
                                    <h3 class="text-center font-weight-light my-4">Add New Vehicle</h3>
                                        <p>Booking Connects Available: <?=$subs[0]['available_connects']?> | <a target="_blank" href="{{  url('/packages/') }}">Get More Connects</a></p>
                                </div>
                                <div class="card-body">
                                    <form id="addvehicle" enctype="multipart/form-data">
                                            @csrf
                                                <div class="row mb-3">
                                                    <div class="col-md-12">
                                                        <div class="form-floating">
                                                            <input class="form-control" id="inputLastName" type="text" name="vehicleName" placeholder="Enter Name of the venue" />
                                                            <label for="inputLastName">Vehicle Name*</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-floating mb-3 mb-md-0">
                                                            <select class="form-control" style="width:100%;" name="vehicleCity" id="">
                                                                <option selected value="Karachi">Karachi</option>
                                                                <option value="Lahore">Lahore</option>
                                                                <option value="Islamabad-Rawalpindi">Islamabad-Rawalpindi</option>
                                                                <option value="Peshawar">Peshawar</option>
                                                                <option value="Faisalabad">Faisalabad</option>
                                                                <option value="Multan">Multan</option>
                                                                <option value="Gujranwala">Gujranwala</option>
                                                                <option value="Sialkot">Sialkot</option>
                                                            </select>
                                                            <label for="inputPassword">Select City</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-floating">
                                                            <input class="form-control" id="inputLastName" name="vehiclePrice" maxlength="8" type="text" placeholder="Enter Area of the venue" />
                                                            <label for="inputLastName">Price/Frequency</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-floating mb-3">
                                                            <label for="details">Complete Details</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-3 mt-5" >
                                                    <div class="col-md-12">
                                                        <div class="form-floating mb-3">
                                                            <textarea class="form-control" name="vehicleDescription" id="vehicleDescription" cols="30" rows="10"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-3 mt-5" >
                                                    <div class="col-md-12">
                                                        <div class="form-floating mb-3">
                                                            <textarea class="form-control" name="vehicleNotes" id="vehicleNotes" cols="30" rows="10"></textarea>
                                                            <label for="details">Short Notes</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-floating mb-3">
                                                            <input class="form-control" id="vehicleImage" name="vehicleImage" type="file" />
                                                            <label for="inputEmail">Upload Image</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-4 mb-3">
                                            <div class="d-grid">
                                                <button type="button" class="save-vehicle btn btn-primary btn-block">Add Vehicle</button>
                                            </div>
                                            </div>
                                    </form>
                                </div>
                                <?php }?>
                                <?php if(count($subs)>0 && $subs[0]['available_connects']<=0){?>
                                    <div class="card-header">
                                        <h3 class="text-center font-weight-light my-4">Add New Vehicle</h3>
                                    </div>
                                    <?php } if(count($subs)<=0){?>
                                    <div class="card-header">
                                        <h3 class="text-center font-weight-light my-4">Add New Vehicle</h3>
                                        <p>No Bookings Connects Available | <a target="_blank" href="{{  url('/packages/') }}">Get Connects to add Vehicles</a></p>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>

                 <!-- add decor -->
                 <div style="display:none;" class="container sectioncontent add-decor">
                    <div class="row justify-content-center">
                        <div class="col-lg-7">
                            <?php if(count($subs)>0 && $subs[0]['available_connects']>0){?>
                            <div class="card shadow-lg border-0 rounded-lg mt-5">
                                <div class="card-header">
                                    <h3 class="text-center font-weight-light my-4">Add New Decor</h3>
                                        <p>Booking Connects Available: <?=$subs[0]['available_connects'] ?> | <a target="_blank" href="{{  url('/packages/') }}">Get More Connects</a></p>
                                </div>
                                <div class="card-body">
                                    <form id="adddecor" enctype="multipart/form-data">
                                            @csrf
                                                <div class="row mb-3">
                                                    <div class="col-md-12">
                                                        <div class="form-floating">
                                                            <input class="form-control" id="inputLastName" type="text" name="decorName" placeholder="Enter Name of the decor" />
                                                            <label for="inputLastName">Decor Name*</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-floating mb-3 mb-md-0">
                                                            <select class="form-control" style="width:100%;" name="decorCity" id="">
                                                                <option selected value="Karachi">Karachi</option>
                                                                <option value="Lahore">Lahore</option>
                                                                <option value="Islamabad-Rawalpindi">Islamabad-Rawalpindi</option>
                                                                <option value="Peshawar">Peshawar</option>
                                                                <option value="Faisalabad">Faisalabad</option>
                                                                <option value="Multan">Multan</option>
                                                                <option value="Gujranwala">Gujranwala</option>
                                                                <option value="Sialkot">Sialkot</option>
                                                            </select>
                                                            <label for="inputPassword">Select City</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-floating">
                                                            <input class="form-control" id="inputLastName" name="decorPrice" maxlength="8" type="text" placeholder="Enter Area of the decor" />
                                                            <label for="inputLastName">Price/Frequency</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-floating mb-3">
                                                            <label for="details">Complete Details</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-3 mt-5" >
                                                    <div class="col-md-12">
                                                        <div class="form-floating mb-3">
                                                            <textarea class="form-control" name="decorDescription" id="decorDescription" cols="30" rows="10"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-3 mt-5" >
                                                    <div class="col-md-12">
                                                        <div class="form-floating mb-3">
                                                            <textarea class="form-control" name="decorNotes" id="decorNotes" cols="30" rows="10"></textarea>
                                                            <label for="details">Short Notes</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-floating mb-3">
                                                            <input class="form-control" id="decorImage" name="decorImage" type="file" />
                                                            <label for="inputEmail">Upload Image</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-4 mb-3">
                                            <div class="d-grid">
                                                <button type="button" class="save-decor btn btn-primary btn-block">Add Decor</button>
                                            </div>
                                            </div>
                                    </form>
                                </div>
                                <?php }?>
                                <?php if(count($subs)>0 && $subs[0]['available_connects']<=0){?>
                                    <div class="card-header">
                                        <h3 class="text-center font-weight-light my-4">Add New Decor</h3>
                                    </div>
                                    <?php } if(count($subs)<=0){?>
                                    <div class="card-header">
                                        <h3 class="text-center font-weight-light my-4">Add New Decor</h3>
                                        <p>No Bookings Connects Available | <a target="_blank" href="{{  url('/packages/') }}">Get Connects to add Decors</a></p>
                                    </div>
                                <?php }?>
                        </div>
                    </div>
                </div>

                <!-- show vehicles -->
                <div style="display:none;" class="container sectioncontent view-vehicles">
                    <h1 class="mt-4">All Vehicles</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Vehicles</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <table id="vehicletable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Short Description</th>
                                            <th>Price/Frequency</th>
                                            <th>City</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Short Description</th>
                                            <th>Price/Frequency</th>
                                            <th>City</th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($vehicles as $venue)
                                        <tr>
                                            <?php
                                                $url=url('/vehicle/'.$venue->id);
                                                $service_name='<a href="'.$url.'">'.$venue->name.'</a>';
                                            ?>
                                            <td>{{ $venue->id}}</td>
                                            <td><?=$service_name?></td>
                                            <td>
                                                <button type="button" onclick="seeDetails('<?=$venue->id?>',this)" class="btn btn-info">Details</button>
                                                <input type="hidden" class="description" value="{{ $venue->description}}">
                                            </td>
                                            <td>{{ $venue->notes}}</td>
                                            <td>{{ $venue->price}}</td>
                                            <td>{{ $venue->city}}</td>
                                            <td>{{ \Carbon\Carbon::parse($venue->created_at)->format('d/m/Y g:i:s')}}</td>
                                            <td>{{ \Carbon\Carbon::parse($venue->updated_at)->format('d/m/Y g:i:s')}}</td>
                                            <td style="width:10%">
                                            <ul class="list-inline m-0">
                                                {{-- <li class="list-inline-item">
                                                    <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                                </li> --}}
                                                <li class="list-inline-item">
                                                    <button class="btn btn-danger btn-sm rounded-0" onclick="deleteItem('{{ $venue->name}}','{{ $venue->id}}',this,'vehicle')" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
                                                </li>
                                            </ul>
                                        </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>

                     <!-- show vehicles -->
                     <div style="display:none;" class="container sectioncontent view-decors">
                        <h1 class="mt-4">All Decors</h1>
                            <ol class="breadcrumb mb-4">
                                <li class="breadcrumb-item active">Decors</li>
                            </ol>
                            <div class="card mb-4">
                                <div class="card-body">
                                    <table id="decortable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Short Description</th>
                                                <th>Price/Frequency</th>
                                                <th>City</th>
                                                <th>Created at</th>
                                                <th>Updated at</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Short Description</th>
                                                <th>Price/Frequency</th>
                                                <th>City</th>
                                                <th>Created at</th>
                                                <th>Updated at</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @foreach($decors as $venue)
                                            <tr>
                                                <?php
                                                    $url=url('/decor/'.$venue->id);
                                                    $service_name='<a href="'.$url.'">'.$venue->name.'</a>';
                                                ?>
                                                <td>{{ $venue->id}}</td>
                                                <td><?=$service_name?></td>
                                                <td>
                                                    <button type="button" onclick="seeDetails('<?=$venue->id?>',this)" class="btn btn-info">Details</button>
                                                    <input type="hidden" class="description" value="{{ $venue->description}}">
                                                </td>
                                                <td>{{ $venue->notes}}</td>
                                                <td>{{ $venue->price}}</td>
                                                <td>{{ $venue->city}}</td>
                                                <td>{{ \Carbon\Carbon::parse($venue->created_at)->format('d/m/Y g:i:s')}}</td>
                                                <td>{{ \Carbon\Carbon::parse($venue->updated_at)->format('d/m/Y g:i:s')}}</td>
                                                <td style="width:10%">
                                                <ul class="list-inline m-0">
                                                    {{-- <li class="list-inline-item">
                                                        <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                                    </li> --}}
                                                    <li class="list-inline-item">
                                                        <button class="btn btn-danger btn-sm rounded-0" onclick="deleteItem('{{ $venue->name}}','{{ $venue->id}}',this,'decor')" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
                                                    </li>
                                                </ul>
                                            </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>

                <!-- show payments -->
                <div style="display:none;" class="container sectioncontent show-payments">
                    <h1 class="mt-4">Payment List</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active">Payments</li>
                    </ol>
                    <div class="card mb-4">
                        <div class="card-body">
                            <table id="paymenttable">
                                <thead>
                                    <tr>
                                        <th>Name on Card</th>
                                        <th>Card Number</th>
                                        <th>Method</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Created at</th>
                                        <th>Updated at</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Name on Card</th>
                                        <th>Card Number</th>
                                        <th>Method</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Created at</th>
                                        <th>Updated at</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach ($payments as $pay )
                                        <tr>
                                            <td>{{ $pay->name_on_card}}</td>
                                            <td>XXXX-XXXX-XXXX-{{ substr($pay->card_number,12,16)}}</td>
                                            <td>Visa</td>
                                            <td>{{ $pay->amount}}</td>
                                            <td>Paid</td>
                                            <td>{{ \Carbon\Carbon::parse($pay->created_at)->format('d/m/Y g:i:s')}}</td>
                                            <td>{{ \Carbon\Carbon::parse($pay->updated_at)->format('d/m/Y g:i:s')}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- show bookings -->
                <div style="display:none;" class="container sectioncontent show-bookings">
                    <h1 class="mt-4">Booking List</h1>
                    <ol class="breadcrumb mb-4">

                        <li class="breadcrumb-item active">Bookings</li>
                    </ol>
                    <div class="card mb-4">
                        <div class="card-body">
                            @if (isset($bookings) > 0)
                            <table id="bookingTable">
                                <thead>
                                    <tr>
                                        <th>Booking Id</th>
                                        <th>Customer</th>
                                        <th>Name</th>
                                        <th>Booking Date</th>
                                        <th>Status</th>
                                        <th>Created at</th>
                                        <th>Updated at</th>
                                        <th colspan=3>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Booking Id</th>
                                        <th>Customer</th>
                                        <th>Name</th>
                                        <th>Booking Date</th>
                                        <th>Status</th>
                                        <th>Created at</th>
                                        <th>Updated at</th>
                                        <th colspan=3>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <script>
                                    var tentative =[];
                                    </script>
                                        @foreach($bookings as $key => $value)
                                        <?php
                                        $array = json_decode(json_encode($usersWithDeleted), true);
                                            foreach($array as $user){
                                                if($user['id']==$value->user_id){
                                                    $user_name=$user['username'];
                                                }
                                            }
                                        if($value->booking_category=="venue"){
                                            $array = json_decode(json_encode($venuesWithDeleted), true);
                                                foreach($array as $valueArray){
                                                    if($valueArray['id']==$value->booked_id){
                                                        if($valueArray['deleted']==0){
                                                            $url=url('/venue/'.$value->booked_id);
                                                            $service_name='<a href="'.$url.'">'.$valueArray['name'].'</a>';
                                                        }else{
                                                            $service_name=$valueArray['name'];
                                                        }

                                                    }
                                                }
                                        }
                                        if($value->booking_category=="vehicle"){
                                        $array = json_decode(json_encode($vehiclesWithDeleted), true);
                                            foreach($array as $valueArray){
                                                if($valueArray['id']==$value->booked_id){
                                                    if($valueArray['deleted']==0){
                                                    $url=url('/vehicles/'.$value->booked_id);
                                                    $service_name=$valueArray['name'];
                                                    $service_name='<a href="'.$url.'">'.$valueArray['name'].'</a>';
                                                    }else{
                                                        $service_name=$valueArray['name'];
                                                    }
                                                }
                                            }
                                        }
                                        if($value->booking_category=="decor"){
                                        $array = json_decode(json_encode($decorsWithDeleted), true);
                                            foreach($array as $valueArray){
                                                if($valueArray['id']==$value->booked_id){
                                                    if($valueArray['deleted']==0){
                                                    $url=url('/decors/'.$value->booked_id);
                                                    $service_name=$valueArray['name'];
                                                    $service_name='<a href="'.$url.'">'.$valueArray['name'].'</a>';
                                                    }else{
                                                        $service_name=$valueArray['name'];
                                                    }
                                                }
                                            }
                                        }
                                        if($value->approvalStatus=="Accepted"){
                                            $status='<span class="badge badge-success">Accepted</span>';
                                        }else if($value->approvalStatus=="user_cancel"){
                                            $status='<span class="badge badge-danger">'.$user_name.' Cancelled</span>';
                                        }else if($value->approvalStatus=="vendor_cancel"){
                                            $status='<span class="badge badge-danger">You Cancelled</span>';
                                        }else{
                                            $status='<span class="badge badge-warning">Pending</span>';
                                        }
                                        ?>
                                        <script>
                                            tentative.push({
                                                    title: `<span style="color:#000; font-size:16px;"><?=$status?></span>&nbsp;<a style="float: right;padding:4px;" href="javascript:void(0)" onclick="showBooking('<?=$value->booking_date?>')"><i style="color:#000; font-size:20px;" class="fa fa-eye"></i></a>`,
                                                    start: "<?=$value->booking_date?>"
                                            });
                                        </script>
                                        <tr class="booking_<?=$value->booking_date?>">
                                            <td><?=$value->id?></td>
                                            <td><a href="{{ url('/view-profile/') }}<?='/'.$value->user_id?>"><?=$user_name?></a></td>
                                            <td><?=$service_name?></td>
                                            <td><?=$value->booking_date?></td>
                                            <td class="status"><?=$status?></td>
                                            <td><?=$value->created_at?></td>
                                            <td><?=$value->updated_at?></td>
                                            <td>
                                                <button type="button" onclick="seeDetails('<?=$value->id?>',this)" class="btn btn-info">Details</button>
                                                <input type="hidden" class="description" value="<?=$value->bookingDescription?>">
                                            </td>
                                            <td>
                                                <button type="button" onclick="bookingCancel('<?=$user_name?>','<?=$value->id?>',this)" class="btn btn-danger">Cancel</button>
                                            </td>
                                            <td>
                                                <button type="button" onclick="bookingAccept('<?=$user_name?>','<?=$value->id?>',this)" class="btn btn-success">Accept</button>
                                            </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- show tentative -->
                <div style="display:none;" class="container sectioncontent show-tentative">
                    <h1 class="mt-4">Booking Calendar</h1>
                    <label id="error" style="color:red;"></label>
                    <div class="card mb-4">
                        <div id='calendar-container'>
                            <div id='calendar'></div>
                          </div>

                            <script src="{{ url('/') }}/assets/calendar/js/jquery-3.3.1.min.js"></script>
                            <script src="{{ url('/') }}/assets/calendar/js/popper.min.js"></script>
                            <script src="{{ url('/') }}/assets/calendar/js/bootstrap.min.js"></script>
                            <script src='{{ url('/') }}/assets/calendar/fullcalendar/packages/core/main.js'></script>
                            <script src='{{ url('/') }}/assets/calendar/fullcalendar/packages/interaction/main.js'></script>
                            <script src='{{ url('/') }}/assets/calendar/fullcalendar/packages/daygrid/main.js'></script>
                            <script src='{{ url('/') }}/assets/calendar/fullcalendar/packages/timegrid/main.js'></script>
                            <script src='{{ url('/') }}/assets/calendar/fullcalendar/packages/list/main.js'></script>
                            <script>
                              document.addEventListener('DOMContentLoaded', function() {
                            var calendarEl = document.getElementById('calendar');

                            var calendar = new FullCalendar.Calendar(calendarEl, {
                              plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
                              height: 'parent',
                              header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'dayGridMonth,timeGridWeek,timeGridDay'
                              },
                              defaultView: 'dayGridMonth',
                              defaultDate: '2022-07-30',
                              navLinks: true, // can click day/week names to navigate views
                              editable: true,
                              eventLimit: true, // allow "more" link when too many events
                              events: tentative
                            });

                            calendar.render();
                          });

                            </script>

                            <script src="{{ url('/') }}/assets/calendar/js/main.js"></script>
                    </div>
                </div>
            </main>

        </div>

        {{-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Eventeous 2022</div>
                </div>
            </div>
        </footer> --}}
    </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    {{-- <script src="js/scripts.js"></script> --}}
    <script src="{{ url('/') }}/assets/js/table-script.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="{{ url('/') }}/assets/js/table-simple-demo.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
	<script src="{{ url('/') }}/assets/login-register/sweetalert2@11.js"></script>
</body>

</html>
<?php if(count($subs)>0 && $subs[0]['available_connects']>0){?>
<script>
    $(document).ready(function() {
        CKEDITOR.replace( 'venueDescription' );
        CKEDITOR.replace( 'vehicleDescription' );
        CKEDITOR.replace( 'decorDescription' );

        $('.fc-title').each(function (){
            $(this).html( $(this).text());
        });

        $('.show-tentative button').click(function (){
            setTimeout(function() {
                $('.fc-title').each(function (){
                $(this).html( $(this).text());
                console.log("test");
            })
             }, 5);
        })
    });
</script>
<?php } ?>
<script type="text/javascript">

    function showSection(indexClass) {
        $('.sectioncontent').hide();
        $('.' + indexClass).show();
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".save-venue").click(function(e){
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var venueName = $("input[name=venueName]").val();
        var venuePrice = $("input[name=venuePrice]").val();
        let formData = new FormData($('#addVenue')[0]);
		if(venueName=="" || venuePrice==""){
			Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Fields with * are required'
			})
		}else{
			$.ajax({
                type: 'post',
                url:"{{ url('save-venue') }}",
                data:formData,
                contentType:false,
                processData:false,
                success:function(data){
				Swal.fire({
						icon: 'success',
						title: 'Good job!',
						text: 'Venue added successfully'
					});
                $('#addVenue')[0].reset();
                location.reload();
                CKEDITOR.instances[instance].setData("");
			   },
			   error: function (error) {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Server Error'
					})
				}
			});
		}
    });


    $(".save-vehicle").click(function(e){
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var vehicleName = $("input[name=vehicleName]").val();
        var vehiclePrice = $("input[name=vehiclePrice]").val();
        let formData = new FormData($('#addvehicle')[0]);
		if(vehicleName=="" || vehiclePrice==""){
			Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Fields with * are required'
			})
		}else{
			$.ajax({
                type: 'post',
                url:"{{ url('save-vehicle') }}",
                data:formData,
                contentType:false,
                processData:false,
                success:function(data){
				Swal.fire({
						icon: 'success',
						title: 'Good job!',
						text: 'vehicle added successfully'
					});
                $('#addvehicle')[0].reset();
                location.reload();
                CKEDITOR.instances[instance].setData("");
			   },
			   error: function (error) {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Server Error'
					})
				}
			});
		}
    });

    $(".save-decor").click(function(e){
    e.preventDefault();
    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }
    var decorName = $("input[name=decorName]").val();
    var decorPrice = $("input[name=decorPrice]").val();
    let formData = new FormData($('#adddecor')[0]);
    if(decorName=="" || decorPrice==""){
        Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Fields with * are required'
        })
    }else{
        $.ajax({
            type: 'post',
            url:"{{ url('save-decor') }}",
            data:formData,
            contentType:false,
            processData:false,
            success:function(data){
            Swal.fire({
                    icon: 'success',
                    title: 'Good job!',
                    text: 'decor added successfully'
                });
            $('#adddecor')[0].reset();
            location.reload();
            CKEDITOR.instances[instance].setData("");
           },
           error: function (error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Server Error'
                })
            }
        });
    }
});



function seeDetails(id,obj){
    var tr=(obj).closest('tr');
    var text=$(tr).find('input.description').val();
    $('#desc').html(text);
    $('#seeDetails').show();
}

function closeDetails(){
    $('#seeDetails').hide();
}
function closeBooking(){
    $('#seeBooking').hide();
}

function bookingCancel(user,id,obj){
    Swal.fire({
        title: 'Are you sure cancel booking from: '+user,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, cancel it!'
        }).then((result) => {
        if (result.isConfirmed) {
            var tr=(obj).closest('tr');
            $(tr).find('td.status').html('<span class="badge badge-danger">You Cancelled</span>');
            $.ajax({
           type:'POST',
           url:"{{ route('booking.update') }}",
           data:{id:id,status:"vendor_cancel"},
           success:function(data){
            Swal.fire({
                    icon: 'success',
                    title: 'Good job!',
                    text: 'Booking cancelled successfully'
                });
                $('#error').html("Note: Refresh page to see the latest updates");
           },
           error: function (error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Server Error'
                })
            }
        });
        }
    })
}

function bookingAccept(user,id,obj){
    Swal.fire({
        title: 'Are you sure to accept booking from: '+user,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, accept it!'
        }).then((result) => {
        if (result.isConfirmed) {
            var tr=(obj).closest('tr');
            $(tr).find('td.status').html('<span class="badge badge-success">Accepted</span>');
            $.ajax({
           type:'POST',
           url:"{{ route('booking.update') }}",
           data:{id:id,status:"Accepted"},
           success:function(data){
            Swal.fire({
                    icon: 'success',
                    title: 'Good job!',
                    text: 'Booking accepted successfully'
                });
                $('#error').html("Note: Refresh page to see the latest updates");
            var tr=$(obj).closest('tr');
           },
           error: function (error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Server Error'
                })
            }
        });
        }
    })
}

function deleteItem(name,id,obj,category){
        if(category=="venue"){
            var url = "{{ route('delete.venue') }}";
        }
        if(category=="vehicle"){
            var url = "{{ route('delete.vehicle') }}";
        }
        if(category=="decor"){
            var url = "{{ route('delete.decor') }}";
        }
        Swal.fire({
            title: 'Are you sure to delete name: '+name,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.isConfirmed) {
                $(obj).closest('tr').remove();
                $.ajax({
			   type:'POST',
			   url:url,
               data:{id:id},
			   success:function(data){
				Swal.fire({
						icon: 'success',
						title: 'Good job!',
						text: 'Items deleted successfully'
					});
                var tr=$(obj).closest('tr');
			   },
			   error: function (error) {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Server Error'
					})
				}
			});
            }
        })
}

function showBooking(id){
    var html = '';
    $('tr.tr_booking').remove();
    $('tr.booking_'+id).each(function (){
        html+="<tr class='tr_booking'>"+$(this).html()+"</tr>";
    });
    console.log(html)
    $('tr#booking').after(html);
    $('#seeBooking').show();
}

</script>
<div class="modal" style="z-index: 9999999999999999 !important;" id="seeDetails" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" onclick="closeDetails()" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="desc" class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" onclick="closeDetails()" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<div class="modal" id="seeBooking" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="max-width: 990px;" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" onclick="closeBooking()" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div  class="modal-body">
            <table class="table table-bordered" style="width:90%">
                <tr id="booking">
                    <th>Booking Id</th>
                    <th>Customer</th>
                    <th>Name</th>
                    <th>Booking Date</th>
                    <th>Status</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th colspan=3>Action</th>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" onclick="closeBooking()" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
