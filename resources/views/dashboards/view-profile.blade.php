<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="zxx">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Eventeous</title>
    <meta name="description" content="">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{ url('/') }}/assets/apple-touch-icon.png">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/normalize.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/icomoon.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap-select.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/scrollbar.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/jquery.mmenu.all.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/prettyPhoto.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/transitions.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/color.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/responsive.css">
    <script src="{{ url('/') }}/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body style="background: #FF7550" class="tg-home tg-homevone">
@include('header')
<div style="margin-top:100px !important;" class="container rounded bg-white mt-5 mb-5">
@foreach($users as $user)
    <div style="padding:25px;" class="row">
        <div class="tg-sectiontitle">
            <h2> {{$user->username}}</h2>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="p-3">
                <div style="margin-left: 25%;" class="d-flex justify-content-between align-items-center mb-3">
                    <div class="d-flex flex-column align-items-center text-center p-3"><img class="rounded-circle mt-5" width="150px" src="{{ url('/') }}{{'/uploads/users/'.$user->image}}"><span class="font-weight-bold">{{$user->username}}</span></div>
                </div>
                <table style="width:80%" class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">Contact: </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td scope="row">{{$user->contact}}</td>
                      </tr>
                    </tbody>
                </table>
                <table style="width:80%" class="table table-striped">
                    <thead>
                        <tr>
                          <th scope="col">Email: </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td scope="row">{{$user->email}}</td>
                        </tr>
                      </tbody>
                </table>
                <table style="width:80%" class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">Role: </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td scope="row">{{$user->usertype}}</td>
                        </tr>
                      </tbody>
                  </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 p-3">
            @if (count($venues) > 0)
            <div class="row">
                <table class="table table-sm table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Venue</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($venues as $key => $venue)
                    <tr>
                        <th scope="row">{{ ($key+1)}}</th>
                        <td>{{ $venue->name }}</td>
                        <td>
                            <ul class="list-inline m-0">
                                <li class="list-inline-item">
                                    <a href="{{ url('/venue/'.$venue->id) }}" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-eye"></i></a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @endif
            @if (count($vehicles) > 0)
            <div class="row">
                <table class="table table-sm table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Vehicles</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($vehicles as $key => $venue)
                    <tr>
                        <th scope="row">{{ ($key+1)}}</th>
                        <td>{{ $venue->name }}</td>
                        <td>
                            <ul class="list-inline m-0">
                                <li class="list-inline-item">
                                    <a href="{{ url('/vehicle/'.$venue->id) }}" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-eye"></i></a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @endif
            @if (count($decors) > 0)
            <div class="row">
                <table class="table table-sm table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Decors</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($decors as $key => $venue)
                    <tr>
                        <th scope="row">{{ ($key+1)}}</th>
                        <td>{{ $venue->name }}</td>
                        <td>
                            <ul class="list-inline m-0">
                                <li class="list-inline-item">
                                    <a href="{{ url('/decor/'.$venue->id) }}" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-eye"></i></a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @endif
            @if (count($bookings) > 0)
            <h1>User's History</h1>
            <div class="row">
                <table class="table table-sm table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Booking Date</th>
                        <th scope="col">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($bookings as $key => $value)
                        <?php
                        $array = json_decode(json_encode($users), true);
                            foreach($array as $user){
                                if($user['id']==$value->user_id){
                                    $user_name=$user['username'];
                                }
                            }
                        if($value->booking_category=="venue"){
                            $array = json_decode(json_encode($allvenues), true);
                                foreach($array as $valueArray){
                                    if($valueArray['id']==$value->booked_id){
                                        $url=url('/venue/'.$value->booked_id);
                                        $service_name='<a href="'.$url.'">'.$valueArray['name'].'</a>';
                                    }
                                }
                        }
                        if($value->booking_category=="vehicle"){
                        $array = json_decode(json_encode($allvehicles), true);
                            foreach($array as $valueArray){
                                if($valueArray['id']==$value->booked_id){
                                    $url=url('/vehicles/'.$value->booked_id);
                                    $service_name=$valueArray['name'];
                                    $service_name='<a href="'.$url.'">'.$valueArray['name'].'</a>';
                                }
                            }
                        }
                        if($value->booking_category=="decor"){
                        $array = json_decode(json_encode($alldecors), true);
                            foreach($array as $valueArray){
                                if($valueArray['id']==$value->booked_id){
                                    $url=url('/decors/'.$value->booked_id);
                                    $service_name=$valueArray['name'];
                                    $service_name='<a href="'.$url.'">'.$valueArray['name'].'</a>';
                                }
                            }
                        }
                        print_r($value->approvalStatus);
                        if($value->approvalStatus=="Accepted"){
                        $status='<span class="badge badge-success">Accepted</span>';
                        }else if($value->approvalStatus=="user_cancel"){
                            $status='<span class="badge badge-danger">'.$user_name.' Cancelled</span>';
                        }else if($value->approvalStatus=="vendor_cancel"){
                            $status='<span class="badge badge-danger">You Cancelled</span>';
                        }else{
                            $status='<span class="badge badge-warning">Pending</span>';
                        }
                        ?>
                        <tr>
                            <td><?=($key+1)?></td>
                            <td><?=$service_name?></td>
                            <td><?=$value->booking_date?></td>
                            <td><?=$status?></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
@endforeach

</div>

<style>
    body {
        background: rgb(99, 39, 120)
    }

    .form-control:focus {
        box-shadow: none;
        border-color: #BA68C8
    }

    .profile-button {
        background: rgb(99, 39, 120);
        box-shadow: none;
        border: none
    }

    .profile-button:hover {
        background: #682773
    }

    .profile-button:focus {
        background: #682773;
        box-shadow: none
    }

    .profile-button:active {
        background: #682773;
        box-shadow: none
    }

    .back:hover {
        color: #682773;
        cursor: pointer
    }

    .labels {
        font-size: 11px
    }

    .add-experience:hover {
        background: #BA68C8;
        color: #fff;
        cursor: pointer;
        border: solid 1px #BA68C8
    }
</style>

    <!--************************************
			Login Singup End
	*************************************-->
    <script src="{{ url('/') }}/assets/js/vendor/jquery-library.js"></script>
    <script src="{{ url('/') }}/assets/js/vendor/bootstrap.min.js"></script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&language=en"></script>
    <script src="{{ url('/') }}/assets/js/bootstrap-select.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery-scrolltofixed.js"></script>
    <script src="{{ url('/') }}/assets/js/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.mmenu.all.js"></script>
    <script src="{{ url('/') }}/assets/js/packery.pkgd.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.vide.min.js"></script>
    <script src="{{ url('/') }}/assets/js/scrollbar.min.js"></script>
    <script src="{{ url('/') }}/assets/js/prettyPhoto.js"></script>
    <script src="{{ url('/') }}/assets/js/countdown.js"></script>
    <script src="{{ url('/') }}/assets/js/parallax.js"></script>
    <script src="{{ url('/') }}/assets/js/gmap3.js"></script>
    <script src="{{ url('/') }}/assets/js/main.js"></script>
</body>

</html>
