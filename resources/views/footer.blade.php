<!-- start site-footer -->
<footer class="site-footer">
    <div class="lower-footer">
        <div class="container">
            <div class="row">
                <div class="separator"></div>
                <div class="col col-xs-12">
                    <p class="copyright">Copyright &copy; <span id="year"></span> Eventeous All rights
                        reserved</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- end site-footer -->

<script>
    let now = new Date()
    let getYear = now.getFullYear();

    document.getElementById("year").innerHTML = getYear
</script>
