
	<!--************************************
				Loader Start
	*************************************-->
	<div class="loader">
		<div class="span">
			<div class="location_indicator"></div>
		</div>
	</div>
	<!--************************************
				Loader End
	*************************************-->
	<!--************************************
				Loader Start
	*************************************-->
	<div class="loader">
		<div class="span">
			<div class="location_indicator"></div>
		</div>
	</div>
	<!--************************************
				Loader End
	*************************************-->
	<!--************************************
			Mobile Menu Start
	*************************************-->
	<nav id="menu">
		<ul>
			<li><a href="{{ url('/') }}">Home</a></li>
            <li class="{{ Request::is('/') ? 'current-menu-parent' : '' }}">
                <a rel="nofollow" href="{{ url('/') }}">Home</a>
            </li>
            <li class="{{ Request::is('services') ? 'current-menu-parent' : '' }}">
                <a href="javascript:void(0)">Our Services</a>
                <ul>
                    <li><a href="{{ url('/') }}/venues">Venues</a></li>
                    <li><a href="{{ url('/') }}/vehicles">Vehicles</a></li>
                    <li><a href="{{ url('/') }}/decors">Decors</a></li>
				</ul>
            </li>
            <li class="{{ Request::is('about-us') ? 'current-menu-parent' : '' }}">
                <a href="{{ url('/') }}/about-us">About Us</a>
            </li>
            <li class="{{ Request::is('contact-us') ? 'current-menu-parent' : '' }}"><a rel="nofollow"
                    href="{{ url('/') }}/contact-us">Contact Us</a>
            </li>
            <?php if(Auth::check()){?>
            <li class="{{ Request::is('login') ? 'current-menu-parent' : '' }}"><a rel="nofollow"
                href="{{ url('/') }}/dashboard"><?=Auth::user()->username?>'s Dashboard</a>
            </li>
            <li class="{{ Request::is('login') ? 'current-menu-parent' : '' }}"><a rel="nofollow"
                href="{{ url('/') }}/logout">Log Out</a>
            </li>
            <?php }else {?>
            <li class="{{ Request::is('login') ? 'current-menu-parent' : '' }}"><a rel="nofollow"
                href="{{ url('/') }}/login">Login</a>
            </li>
            <li class="{{ Request::is('register') ? 'current-menu-parent' : '' }}"><a rel="nofollow"
                href="{{ url('/') }}/register">Register</a>
            </li>
            <?php }?>
		</ul>
	</nav>
	<!--************************************
			Mobile Menu End
	*************************************-->
	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="tg-wrapper" class="tg-wrapper tg-haslayout">
		<!--************************************
				Header Start
		*************************************-->
		<header id="tg-header" class="tg-header tg-haslayout">
			<div class="container-fluid">
				<div class="row">
					<div class="tg-navigationarea tg-headerfixed">
						<strong class="tg-logo"><a href="{{ url('/') }}"><img src="{{ url('/') }}/assets/images/logo.jpg" width="70" alt="company logo here"></a></strong>
						<nav id="tg-nav" class="tg-nav">
							<div class="navbar-header">
								<a href="#menu" class="navbar-toggle collapsed">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</a>
							</div>
							<div id="tg-navigation" class="collapse navbar-collapse tg-navigation">
								<ul>
									<li class="menu-item-has-children {{ Request::is('home') ? 'current-menu-item' : '' }}">
                                        <a href="{{ url('/') }}/home">Home</a>
									</li>
                                    <li class="menu-item-has-children {{ Request::is('services') ? 'current-menu-parent' : '' }}">
                                        <a href="javascript:void(0)">Our Services</a>
                                        <ul class="sub-menu">
                                            <li><a href="{{ url('/') }}/venues">Venues</a></li>
                                            <li><a href="{{ url('/') }}/vehicles">Vehicles</a></li>
                                            <li><a href="{{ url('/') }}/decors">Decors</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children {{ Request::is('about-us') ? 'current-menu-parent' : '' }}">
                                        <a href="{{ url('/') }}/about-us">About Us</a>
                                    </li>
                                    <li class="menu-item-has-children {{ Request::is('contact-us') ? 'current-menu-parent' : '' }}"><a rel="nofollow"
                                            href="{{ url('/') }}/contact-us">Contact Us</a>
                                    </li>
                                    <?php if(Auth::check()){?>
                                    <li class="menu-item-has-children {{ Request::is('login') ? 'current-menu-parent' : '' }}"><a rel="nofollow"
                                        href="{{ url('/') }}/dashboard"><?=Auth::user()->username?>'s Dashboard</a>
                                    </li>
                                    <li class="menu-item-has-children {{ Request::is('login') ? 'current-menu-parent' : '' }}"><a rel="nofollow"
                                        href="{{ url('/') }}/logout">Log Out</a>
                                    </li>
                                    <?php }else {?>
                                    <li class="menu-item-has-children {{ Request::is('login') ? 'current-menu-parent' : '' }}"><a rel="nofollow"
                                            href="{{ url('/') }}/login">Login</a>
                                    </li>
                                    <li class="menu-item-has-children {{ Request::is('register') ? 'current-menu-parent' : '' }}"><a rel="nofollow"
                                            href="{{ url('/') }}/register">Register</a>
                                    </li>
                                    <?php }?>
								</ul>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</header>
		<!--************************************
				Header End
		*************************************-->
