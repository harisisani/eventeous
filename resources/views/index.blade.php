<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="zxx">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Eventeous</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{ url('/') }}/assets/apple-touch-icon.png">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/normalize.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/icomoon.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap-select.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/scrollbar.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/jquery.mmenu.all.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/prettyPhoto.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/transitions.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/color.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/responsive.css">
    <script src="{{ url('/') }}/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body class="tg-home tg-homevone">
    @include('header')
    <!--************************************
				Home Slider Start
		*************************************-->
    <div class="tg-bannerholder">
        <div class="tg-slidercontent">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h1>WELCOME TO EVENTEOUS</h1>
                        <h2>Will arrange all the necessities for your event</h2>
                        <form id="searchForm" enctype="multipart/form-data" action="{{ url('/search') }}"  method="POST" class="tg-formtheme tg-formtrip">
                            @csrf
                                <div style="width:25%;" class="form-group">
                                    <input type="text" name="searchKeyword" id="searchKeyword" style="width:100%;height: 50px;" placeholder="Keyword">
                                </div>
                                <div style="width:25%;" class="form-group">
                                    <div class="tg-select">
                                        <select class="selectpicker" name="searchCategory" id="searchCategory" data-live-search="true" data-width="100%">
                                            <option selected value="venues" >Venues</option>
                                            <option value="vehicles">Vehicles</option>
                                            <option value="decors">Decors</option>
                                        </select>
                                    </div>
                                </div>
                                <div style="width:25%;" class="form-group">
                                    <div class="tg-select">
                                        <select class="selectpicker" name="searchCity" id="searchCity" data-live-search="true" data-width="100%">
                                            <option selected value="Karachi">Karachi</option>
                                            <option value="Lahore">Lahore</option>
                                            <option value="Islamabad-Rawalpindi">Islamabad-Rawalpindi</option>
                                            <option value="Peshawar">Peshawar</option>
                                            <option value="Faisalabad">Faisalabad</option>
                                            <option value="Multan">Multan</option>
                                            <option value="Gujranwala">Gujranwala</option>
                                            <option value="Sialkot">Sialkot</option>
                                        </select>
                                    </div>
                                </div>
                                <div style="width:25%;" class="form-group">
                                    <button style="position:inherit;" class="tg-btn" type="submit">Search</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function doSearch(){
                $('#searchForm').submit();
                var formData = new FormData($('#searchForm')[0]);
                $.ajax({
                type: 'post',
                url:"{{ url('search') }}",
                data:formData,
                contentType:false,
                processData:false,
                success:function(data){
			   },
			   error: function (error) {
			}
			});
            }
        </script>
        <div id="tg-homeslider" class="tg-homeslider owl-carousel tg-haslayout">
            <figure class="item" data-vide-bg="poster: {{ url('/') }}/assets/images/slider/img-01.jpg" data-vide-options="position: 0% 50%"></figure>
            <figure class="item" data-vide-bg="poster: {{ url('/') }}/assets/images/slider/img-02.jpg" data-vide-options="position: 0% 50%"></figure>
            <figure class="item" data-vide-bg="poster: {{ url('/') }}/assets/images/slider/img-03.jpg" data-vide-options="position: 0% 50%"></figure>
        </div>
    </div>
    <!--************************************
				Home Slider End
		*************************************-->
    <!--************************************
				Main Start
		*************************************-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <!--************************************
					Advantures Start
			*************************************-->
        <section class="tg-sectionspace tg-haslayout">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="tg-sectionhead tg-sectionheadvtwo">
                    <div class="tg-sectiontitle">
                        <h2>Why Choose Eventeous</h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-toursdestinations">
                            <div class="tg-tourdestination tg-tourdestinationbigbox">
                                <figure>
                                    <a href="{{ url('/') }}/venues">
                                        <img src="{{ url('/') }}/assets/images/destination/img-01.jpg" alt="image destinations">
                                        <div class="tg-hoverbox">
                                            <div class="tg-adventuretitle">
                                                <h2>Beautiful Venues</h2>
                                            </div>
                                            <div class="tg-description">
                                                <p>your best event ever</p>
                                            </div>
                                        </div>
                                    </a>
                                </figure>
                            </div>
                            <div class="tg-tourdestination">
                                <figure>
                                    <a href="{{ url('/') }}/vehicles">
                                        <img src="{{ url('/') }}/assets/images/destination/img-02.jpg" alt="image destinations">
                                        <div class="tg-hoverbox">
                                            <div class="tg-adventuretitle">
                                                <h2>Rent a car</h2>
                                            </div>
                                        </div>
                                    </a>
                                </figure>
                            </div>
                            <div class="tg-tourdestination">
                                <figure>
                                    <a href="{{ url('/') }}/decors">
                                        <img src="{{ url('/') }}/assets/images/destination/img-03.jpg" alt="image destinations">
                                        <div class="tg-hoverbox">
                                            <div class="tg-adventuretitle">
                                                <h2>Decorate your event</h2>
                                            </div>
                                        </div>
                                    </a>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--************************************
					Recent Posts Section Start
		*************************************-->
        <section class="tg-sectionspace tg-zerotoppadding tg-haslayout">

            <div class="container">
                <div class="row">
                    <div class="tg-features">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="tg-feature">
                                <div class="tg-featuretitle">
                                    <h2><span>01</span>Why Eventeous</h2>
                                </div>
                                <div class="tg-description">
                                    <p>The Eventeous came forward as a rescue to users that are in search of an easy-going online platform to form events dimensions. It is a public responsive & dynamic web portal application that is designed & developed to provide the customer an ease of access to book any thing related to events such as venues, logistics, vendors etc. The application is developed with an attractive User Interface as well as easy-going User Experience.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="tg-feature">
                                <div class="tg-featuretitle">
                                    <h2><span>02</span>What is Eventeous</h2>
                                </div>
                                <div class="tg-description">
                                    <p>The Eventeous is an effective web-based system. It overcomes the hassle of searching venues, transport programs, vendors, etc & provide you a source of connection with an ease of access to the available options with just a fingers tip away.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="tg-feature">
                                <div class="tg-featuretitle">
                                    <h2><span>03</span>Our Team</h2>
                                </div>
                                <div class="tg-description">
                                    <p>Team Eventeous believes on uninterrupted growth, the reason we are fully focused on constant growth & scaling of the project. We are in consistent research & development to make it big. With public figure, support & community, Eventeous will eventually become a brand & can be a regularity for customers & a tough competition in the market for the competitors.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--************************************
					Recent Posts Section End
		*************************************-->
        <section class="tg-parallax" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ url('/') }}/assets/images/parallax/bgparallax-01.jpg">
            <div class="tg-sectionspace tg-haslayout">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="tg-sectiontitle tg-sectiontitleleft">
                                <h2>Recent Posts</h2>
                            </div>
                            <div id="tg-populartoursslider" class="tg-populartoursslider tg-populartours owl-carousel">
                                @foreach($popularVenues as $venue)
                                    <?php
                                        $days = (int) ((strtotime(now()) - strtotime($venue->created_at)) / (60 * 60 * 24));
                                        if($days>=1){
                                                $posted="Posted: ".$days." day(s) ago";
                                        }else{
                                            $hours = (int) ((strtotime(now()) - strtotime($venue->created_at)) / (60 * 60));
                                                $posted="Posted: ".$hours." hour(s) ago";
                                        }
                                    ?>
                                <div class="item tg-populartour">
                                    <figure>
                                        <a href="{{ url('/venue/'.$venue->id) }}"><img style="height:250px;"  src="{{ url('/') }}{{'/uploads/venues/'.$venue->picture_links}}" alt="image destinations"></a>
                                        <!-- <span class="tg-descount">25% Off</span> -->
                                    </figure>
                                    <div class="tg-populartourcontent">
                                        <div class="tg-populartourtitle">
                                            <h3><a href="{{ url('/venue/'.$venue->id) }}">{{$venue->name}}</a></h3>
                                        </div>
                                        <div class="tg-description">
                                            <p>{{$venue->notes}}</p>
                                        </div>
                                        <div class="tg-description">
                                            <p><?=$posted?></p>
                                        </div>
                                        <div class="tg-populartourfoot">
                                            <div class="tg-pricearea">
                                                <h4>{{$venue->price}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @foreach($popularVehicles as $venue)
                                    <?php
                                        $days = (int) ((strtotime(now()) - strtotime($venue->created_at)) / (60 * 60 * 24));
                                        if($days>=1){
                                                $posted="Posted: ".$days." day(s) ago";
                                        }else{
                                            $hours = (int) ((strtotime(now()) - strtotime($venue->created_at)) / (60 * 60));
                                                $posted="Posted: ".$hours." hour(s) ago";
                                        }
                                    ?>
                                <div class="item tg-populartour">
                                    <figure>
                                        <a href="{{ url('/vehicle/'.$venue->id) }}"><img style="height:250px;"  src="{{ url('/') }}{{'/uploads/vehicles/'.$venue->picture_links}}" alt="image destinations"></a>
                                        <!-- <span class="tg-descount">25% Off</span> -->
                                    </figure>
                                    <div class="tg-populartourcontent">
                                        <div class="tg-populartourtitle">
                                            <h3><a href="{{ url('/vehicle/'.$venue->id) }}">{{$venue->name}}</a></h3>
                                        </div>
                                        <div class="tg-description">
                                            <p>{{$venue->notes}}</p>
                                        </div>
                                        <div class="tg-description">
                                            <p><?=$posted?></p>
                                        </div>
                                        <div class="tg-populartourfoot">
                                            <div class="tg-pricearea">
                                                <h4>{{$venue->price}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @foreach($popularDecors as $venue)
                                    <?php
                                        $days = (int) ((strtotime(now()) - strtotime($venue->created_at)) / (60 * 60 * 24));
                                        if($days>=1){
                                                $posted="Posted: ".$days." day(s) ago";
                                        }else{
                                            $hours = (int) ((strtotime(now()) - strtotime($venue->created_at)) / (60 * 60));
                                                $posted="Posted: ".$hours." hour(s) ago";
                                        }
                                    ?>
                                <div class="item tg-populartour">
                                    <figure>
                                        <a href="{{ url('/decor/'.$venue->id) }}"><img style="height:250px;" src="{{ url('/') }}{{'/uploads/decor/'.$venue->picture_links}}" alt="image destinations"></a>
                                        <!-- <span class="tg-descount">25% Off</span> -->
                                    </figure>
                                    <div class="tg-populartourcontent">
                                        <div class="tg-populartourtitle">
                                            <h3><a href="{{ url('/decor/'.$venue->id) }}">{{$venue->name}}</a></h3>
                                        </div>
                                        <div class="tg-description">
                                            <p>{{$venue->notes}}</p>
                                        </div>
                                        <div class="tg-description">
                                            <p><?=$posted?></p>
                                        </div>
                                        <div class="tg-populartourfoot">
                                            <div class="tg-pricearea">
                                                <h4>{{$venue->price}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--************************************
					Venues Section Start
		*************************************-->
        @if (count($venues) > 0)
        <section class="tg-sectionspace tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-sectionhead tg-sectionheadvtwo">
                            <div class="tg-sectiontitle">
                                <h2>Venues</h2>
                            </div>
                            <div class="tg-description">
                                <p>Book your favourite venue now</p>
                            </div>
                        </div>
                    </div>
                    <div class="tg-trendingtrips">
                        @foreach($venues as $venue)
                        <?php
                           $days = (int) ((strtotime(now()) - strtotime($venue->created_at)) / (60 * 60 * 24));
                           if($days>=1){
                                $posted="Posted: ".$days." day(s) ago";
                           }else{
                            $hours = (int) ((strtotime(now()) - strtotime($venue->created_at)) / (60 * 60));
                                $posted="Posted: ".$hours." hour(s) ago";
                           }
                        ?>
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <div class="tg-trendingtrip">
                                <div class="tg-description">
                                    <p><?php echo $posted ?></p>
                                </div>
                                <figure>
                                    <a href="{{ url('/venue/'.$venue->id) }}">
                                        <img style="height:250px;" src="{{ url('/') .'/uploads/venues/'.$venue->picture_links}}" alt="image destinations">
                                        <div class="tg-hover">
                                            {{-- <span class="tg-stars"><span></span></span> --}}
                                            {{-- <span class="tg-tourduration">5 hours</span> --}}
                                            <span class="tg-locationname">{{$venue->city}}</span>
                                            <div class="tg-pricearea">
                                                <span>Rent per day</span>
                                                <h4>{{$venue->price}}</h4>
                                            </div>
                                        </div>
                                    </a>
                                </figure>
                                <div class="tg-populartourcontent">
                                    <div class="tg-populartourtitle">
                                        <h3><a href="{{ url('/venue/'.$venue->id) }}">{{$venue->name}}</a></h3>
                                    </div>
                                    <div class="tg-description">
                                        <p>{{($venue->notes) }}</p>
                                    </div>
                                    <div style="text-align: right;" class="tg-description">
                                        <?php
                                        $array = json_decode(json_encode($users), true);
                                            foreach($array as $user){
                                                if($user['id']==$venue->vendors_id){
                                                    $vendor_name=$user['username'];
                                                    $image=$user['image'];
                                                }
                                            }
                                        ?>
                                        <p>Posted by: <a href="{{ url('/') }}/view-profile/{{ $venue->vendors_id }}"><span><img class="rounded-circle" width="20px" src="{{ url('/') }}{{'/uploads/users/'.$image}}">&nbsp;{{ $vendor_name }}</span></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div  class="tg-pkgplanfoot">
                            <a style="float: right;" class="tg-btn" href="{{ url('/') }}/venues"><span>see all venues</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--************************************
					Venues Section End
		*************************************-->
        @endif
        <!--************************************
					Vehicles Section End
		*************************************-->
        @if (count($vehicles) > 0)
        <section class="tg-sectionspace tg-haslayout" style="background: #f7f7f7;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-sectionhead tg-sectionheadvtwo">
                            <div class="tg-sectiontitle">
                                <h2>Cars</h2>
                            </div>
                            <div class="tg-description">
                                <p>Book your car now</p>
                            </div>
                        </div>
                    </div>
                    <div class="tg-trendingtrips">
                        @foreach($vehicles as $v)
                        <?php
                           $days = (int) ((strtotime(now()) - strtotime($v->created_at)) / (60 * 60 * 24));
                           if($days>=1){
                                $posted="Posted: ".$days." day(s) ago";
                           }else{
                            $hours = (int) ((strtotime(now()) - strtotime($v->created_at)) / (60 * 60));
                                $posted="Posted: ".$hours." hour(s) ago";
                           }
                        ?>
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <div class="tg-trendingtrip">
                                <div class="tg-description">
                                    <p><?php echo $posted ?></p>
                                </div>
                                <figure>
                                    <a href="{{ url('/vehicle/'.$v->id) }}">
                                        <img style="height:250px;"  src="{{ url('/') }}{{'/uploads/vehicles/'.$v->picture_links}}" alt="image destinations">
                                        <div class="tg-hover">
                                            {{-- <span class="tg-stars"><span></span></span> --}}
                                            {{-- <span class="tg-tourduration">24 hours</span> --}}
                                            <span class="tg-locationname">{{$v->city}}</span>
                                            <div class="tg-pricearea">
                                                <span>from</span>
                                                <h4>Rs {{$v->price}}</h4>
                                            </div>
                                        </div>
                                    </a>
                                </figure>
                                <div class="tg-populartourcontent">
                                    <div class="tg-populartourtitle">
                                        <h3><a href="{{ url('/vehicle/'.$v->id) }}">{{$v->name}}</a></h3>
                                    </div>
                                    <div class="tg-description">
                                        <p>{{$v->notes}}</p>
                                    </div>
                                    <div style="text-align: right;" class="tg-description">
                                        <?php
                                        $array = json_decode(json_encode($users), true);
                                            foreach($array as $user){
                                                if($user['id']==$v->vendors_id){
                                                    $vendor_name=$user['username'];
                                                    $image=$user['image'];
                                                }
                                            }
                                        ?>
                                        <p>Posted by: <a href="{{ url('/') }}/view-profile/{{ $v->vendors_id }}"><span><img class="rounded-circle" width="20px" src="{{ url('/') }}{{'/uploads/users/'.$image}}">&nbsp;{{ $vendor_name }}</span></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div  class="tg-pkgplanfoot">
                            <a style="float: right;" class="tg-btn" href="{{ url('/') }}/vehicles"><span>see all vehicles</span></a>
                        </div>
                    </div>


                </div>
            </div>
        </section>
        <!--************************************
					Vehicles Section End
		*************************************-->
        @endif
        @if (count($decors) > 0)
        <!--************************************
					Decors Section Start
		*************************************-->
        <section class="tg-sectionspace tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-sectionhead tg-sectionheadvtwo">
                            <div class="tg-sectiontitle">
                                <h2>Decors</h2>
                            </div>
                            <div class="tg-description">
                                <p>Decorate your events</p>
                            </div>
                        </div>
                    </div>
                    <div class="tg-trendingtrips">
                        @foreach($decors as $d)
                        <?php
                        $days = (int) ((strtotime(now()) - strtotime($d->created_at)) / (60 * 60 * 24));
                        if($days>=1){
                             $posted="Posted: ".$days." day(s) ago";
                        }else{
                         $hours = (int) ((strtotime(now()) - strtotime($d->created_at)) / (60 * 60));
                             $posted="Posted: ".$hours." hour(s) ago";
                        }
                     ?>
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <div class="tg-trendingtrip">
                                <div class="tg-description">
                                    <p><?php echo $posted ?></p>
                                </div>
                                <figure>
                                    <a href="{{ url('/decor/'.$d->id) }}">
                                        <img style="height:250px;" src="{{ url('/') }}{{'/uploads/decor/'.$d->picture_links}}" alt="image destinations">
                                        <div class="tg-hover">
                                            {{-- <span class="tg-stars"><span></span></span> --}}
                                            <!-- <span class="tg-tourduration">5 hours</span> -->
                                            <span class="tg-locationname">{{$d->city}}</span>
                                            <div class="tg-pricearea">
                                                <span>from</span>
                                                <h4>Rs {{$d->price}}</h4>
                                            </div>
                                        </div>
                                    </a>
                                </figure>
                                <div class="tg-populartourcontent">
                                    <div class="tg-populartourtitle">
                                        <h3><a href="{{ url('/decor/'.$d->id) }}">{{$d->name}}</a></h3>
                                    </div>
                                    <div class="tg-description">
                                        <p>{{$d->notes}}</p>
                                    </div>
                                    <div style="text-align: right;" class="tg-description">
                                        <?php
                                        $array = json_decode(json_encode($users), true);
                                            foreach($array as $user){
                                                if($user['id']==$d->vendors_id){
                                                    $vendor_name=$user['username'];
                                                    $image=$user['image'];
                                                }
                                            }
                                        ?>
                                        <p>Posted by: <a href="{{ url('/') }}/view-profile/{{ $d->vendors_id }}"><span><img class="rounded-circle" width="20px" src="{{ url('/') }}{{'/uploads/users/'.$image}}">&nbsp;{{ $vendor_name }}</span></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div  class="tg-pkgplanfoot">
                            <a style="float: right;" class="tg-btn" href="{{ url('/') }}/decors"><span>see all decors</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--************************************
					Decors Section End
		*************************************-->
        @endif
        <!--************************************
					Packages Section
		*************************************-->
        <main id="tg-main" class="tg-main tg-sectionspace tg-haslayout">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="tg-sectionhead tg-sectionheadvtwo">
                    <div class="tg-sectiontitle">
                        <h2>Packages</h2>
                    </div>
                    <div class="tg-description">
                        <p>We provide services on affordable rates</p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-pkgplans">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <div class="tg-pkgplan">
                                            <div class="tg-pkgplantitle">
                                                <h2>Basic</h2>
                                            </div>
                                            <ul class="tg-pkgplanoptions">
                                                <li>Book a Venue</li>
                                                <li>Book a car</li>
                                                <li>Decorate your Event</li>
                                            </ul>
                                            <div class="tg-pkgplanfoot text-center">
                                                <p class="tg-pkgplanprice"><sup>RS</sup>500<sub>/per connect</sub> </p>
                                                <?php
                                                if(Auth::check()){
                                                if(Auth::user()->usertype!="user"){  ?>
                                                    {{-- <br/> <p><a href="{{  url('/logout') }}">Login</a> as a user to Get this offer</p> --}}
                                                    <a class="tg-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter1"><span>Get this Offer</span></a>
                                                <?php } else{
                                                ?>
                                                <a class="tg-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter1"><span>Get this Offer</span></a>
                                                <?php }}else{?>
                                                    <br/> <p><a href="{{  url('/register') }}">Register</a> as a user to Get this offer</p>
                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <div class="tg-pkgplan">
                                            <div class="tg-pkgplantitle">
                                                <h2>Plus</h2>
                                            </div>
                                            <ul class="tg-pkgplanoptions">
                                                <li>Book a Venue</li>
                                                <li>Book a car</li>
                                                <li>Decorate your Event</li>
                                            </ul>
                                                <div class="tg-pkgplanfoot text-center">
                                                    <p class="tg-pkgplanprice"><sup>RS</sup>2000<sub>/5 connects</sub> </p>
                                                    <?php
                                                    if(Auth::check()){
                                                    if(Auth::user()->usertype!="user"){  ?>
                                                        {{-- <br/> <p><a href="{{  url('/logout') }}">Login</a> as a user to Get this offer</p> --}}
                                                        <a class="tg-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter2"><span>Get this Offer</span></a>
                                                    <?php } else{
                                                    ?>
                                                    <a class="tg-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter2"><span>Get this Offer</span></a>
                                                    <?php }}else{?>
                                                        <br/> <p><a href="{{  url('/register') }}">Register</a> as a user to Get this offer</p>
                                                    <?php }?>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <div class="tg-pkgplan">
                                            <div class="tg-pkgplantitle">
                                                <h2>Premium</h2>
                                            </div>
                                            <ul class="tg-pkgplanoptions">
                                                <li>Book a Venue</li>
                                                <li>Book a car</li>
                                                <li>Decorate your Event</li>
                                            </ul>
                                                <div class="tg-pkgplanfoot text-center">
                                                    <p class="tg-pkgplanprice"><sup>RS</sup>3500<sub>/10 connects</sub> </p>
                                                    <?php
                                                    if(Auth::check()){
                                                    if(Auth::user()->usertype!="user"){  ?>
                                                        {{-- <br/> <p><a href="{{  url('/logout') }}">Login</a> as a user to Get this offer</p> --}}
                                                        <a class="tg-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter3"><span>Get this Offer</span></a>
                                                    <?php } else{
                                                    ?>
                                                    <a class="tg-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter3"><span>Get this Offer</span></a>
                                                    <?php }}else{?>
                                                        <br/> <p><a href="{{  url('/register') }}">Register</a> as a user to Get this offer</p>
                                                    <?php }?>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!--************************************
					Packages Section Start
		*************************************-->
    </main>
     <!--************************************
				Footer Start
		*************************************-->
    <footer id="tg-footer" class="tg-footer tg-haslayout">
        <div class="tg-footerbar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p>Copyright &copy; 2022 Eventeous. All rights reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--************************************
				Footer End
		*************************************-->
    </div>
    <script src="{{ url('/') }}/assets/js/vendor/jquery-library.js"></script>
    <script src="{{ url('/') }}/assets/js/vendor/bootstrap.min.js"></script>
    <script src="{{ url('/') }}/assets/js/bootstrap-select.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery-scrolltofixed.js"></script>
    <script src="{{ url('/') }}/assets/js/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.mmenu.all.js"></script>
    <script src="{{ url('/') }}/assets/js/packery.pkgd.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.vide.min.js"></script>
    <script src="{{ url('/') }}/assets/js/scrollbar.min.js"></script>
    <script src="{{ url('/') }}/assets/js/prettyPhoto.js"></script>
    <script src="{{ url('/') }}/assets/js/countdown.js"></script>
    <script src="{{ url('/') }}/assets/js/parallax.js"></script>
    <script src="{{ url('/') }}/assets/js/main.js"></script>
    <script src="{{ url('/') }}/assets/login-register/sweetalert2@11.js"></script>
</body>
</html>


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Get a Booking Connect for PKR: 500/=</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <form id="subs-1">
                    <label style="text-align: center">Fields with * are required</label>
                    <input type="hidden" name="available_connects" value="1">
                    <input type="hidden" name="amount" value="500">
                    <input type="hidden" name="method" value="Visa">
                    <input type="hidden" name="user_id" value="<?=(isset(Auth::user()->id))? Auth::user()->id : '' ?>">
                    <div class="form-group">
                        <label for="inputAddress">Name on Card*</label>
                        <input  type="text" class="form-control" name="name_on_card" placeholder="Example: John Doe">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress2">Card Number<label>
                        <input type="text" maxlength="16"  class="form-control" name="card_number" placeholder="Example: 4012 8888 8888 1881">
                    </div>
                    <div style="width:45%;margin-right:10%;" class="form-group">
                        <label for="inputCity">Date of Expiry*</label>
                        <input maxlength="5" type="text" class="form-control" onkeyup="makeExpiryFormat(this)" placeholder="Example: 02/26" name="expiry">
                    </div>
                    <div style="width:45%;" class="form-group">
                        <label for="inputZip">CVV*</label>
                        <input maxlength="3" type="text" class="form-control" placeholder="Example: 021" name="cvv">
                    </div>
                </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="saveSubscriptions('subs-1')">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Get 5 Bookings Connects for PKR: 2000/=</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="subs-2">
                <label style="text-align: center">Fields with * are required</label>
                <input type="hidden" name="available_connects" value="5">
                <input type="hidden" name="amount" value="2000">
                <input type="hidden" name="method" value="Visa">
                <input type="hidden" name="user_id" value="<?=(isset(Auth::user()->id))? Auth::user()->id : '' ?>">
                <div class="form-group">
                    <label for="inputAddress">Name on Card*</label>
                    <input  type="text" class="form-control" name="name_on_card" placeholder="Example: John Doe">
                </div>
                <div class="form-group">
                    <label for="inputAddress2">Card Number<label>
                    <input type="text" maxlength="16"  class="form-control" name="card_number" placeholder="Example: 4012 8888 8888 1881">
                </div>
                <div style="width:45%;margin-right:10%;" class="form-group">
                    <label for="inputCity">Date of Expiry*</label>
                    <input maxlength="5" type="text" class="form-control" onkeyup="makeExpiryFormat(this)" placeholder="Example: 02/26" name="expiry">
                </div>
                <div style="width:45%;" class="form-group">
                    <label for="inputZip">CVV*</label>
                    <input maxlength="3" type="text" class="form-control" placeholder="Example: 021" name="cvv">
                </div>
            </form>
    </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="saveSubscriptions('subs-2')">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Get 10 Bookings Connects for PKR: 3500/=</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="subs-3">
                <label style="text-align: center">Fields with * are required</label>
                <input type="hidden" name="available_connects" value="10">
                <input type="hidden" name="amount" value="3500">
                <input type="hidden" name="method" value="Visa">
                <input type="hidden" name="user_id" value="<?=(isset(Auth::user()->id))? Auth::user()->id : '' ?>">
                <div class="form-group">
                    <label for="inputAddress">Name on Card*</label>
                    <input  type="text" class="form-control" name="name_on_card" placeholder="Example: John Doe">
                </div>
                <div class="form-group">
                    <label for="inputAddress2">Card Number<label>
                    <input type="text" maxlength="16"  class="form-control" name="card_number" placeholder="Example: 4012 8888 8888 1881">
                </div>
                <div style="width:45%;margin-right:10%;" class="form-group">
                    <label for="inputCity">Date of Expiry*</label>
                    <input maxlength="5" type="text" class="form-control" onkeyup="makeExpiryFormat(this)" placeholder="Example: 02/26" name="expiry">
                </div>
                <div style="width:45%;" class="form-group">
                    <label for="inputZip">CVV*</label>
                    <input maxlength="3" type="text" class="form-control" placeholder="Example: 021" name="cvv">
                </div>
            </form>
    </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="saveSubscriptions('subs-3')">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function saveSubscriptions(formId){
            var card_number = $("#"+formId+' input[name="card_number"]').val();
            var expiry = $("#"+formId+' input[name="expiry"]').val();
            var cvv = $("#"+formId+' input[name="cvv"]').val();
            if(card_number!="" && expiry!="" && cvv!=""){
                if(card_number.length<16 || cvv.length<3 || expiry.length<5){
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Invalid Card Details'
                    });
                }else{
                    $('#exampleModalCenter1').modal('hide');
                    $('#exampleModalCenter2').modal('hide');
                    $('#exampleModalCenter3').modal('hide');
                    let formData = new FormData($('#'+formId)[0]);
                    $.ajax({
                        type: 'post',
                        url:"{{ url('save-subscription') }}",
                        data:formData,
                        contentType:false,
                        processData:false,
                        success:function(data){
                            Swal.fire({
                            icon: 'success',
                            title: 'Good job!',
                            text: 'Connect Added Successfully'
                        });
                        $('#'+formId)[0].reset();
                        },
                    error: function (error) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Server Error'
                            })
                        }
                    });
                }

            }else{
                Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Fill out the required fields'
                    })
            }

        }

        makeExpiryFormat=(obj)=>{
            var value=$(obj).val();
            if(value.length>1 && value.includes('/')==false){
                $(obj).val(value+"/");
            }
        }
  </script>
  <style>
    .modal-backdrop{
        z-index:0;
    }

    .modal-backdrop.in {
        opacity: 0;
    }

    .modal-footer {
        padding: 15px;
        text-align: right;
        border-top:none !important;
    }

</style>
