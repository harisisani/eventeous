<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="zxx">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Eventeous</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{ url('/') }}/assets/apple-touch-icon.png">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/normalize.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/icomoon.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap-select.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/scrollbar.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/jquery.mmenu.all.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/prettyPhoto.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/transitions.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/color.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/responsive.css">
    <script src="{{ url('/') }}/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="tg-home tg-homevone">
    @include('header')
        <main style="padding: 100px 0px 0px 0px;"  id="tg-main" class="tg-main tg-sectionspace tg-haslayout">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="tg-sectionhead tg-sectionheadvtwo">
                    <div class="tg-sectiontitle">
                        <h2>Packages</h2>
                    </div>
                    <div class="tg-description">
                        <p>We provide services on affordable rates</p>
                    </div>
                    <?php if(count($subs)>0 && $subs[0]['available_connects']>0){?>
                        <h3>Booking Connects Available: <?=$subs[0]['available_connects']?></h3>
                    </div>
                    <?php }?>
                        <?php if(count($subs)>0 && $subs[0]['available_connects']<=0){?>
                            <h3>No Bookings Connects Available | Get Connect from below Offers</h3>
                        <?php }?>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-pkgplans">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <div class="tg-pkgplan">
                                            <div class="tg-pkgplantitle">
                                                <h2>Basic</h2>
                                            </div>
                                            <ul class="tg-pkgplanoptions">
                                                <li>Book a Venue</li>
                                                <li>Book a car</li>
                                                <li>Decorate your Event</li>
                                            </ul>
                                            <div class="tg-pkgplanfoot text-center">
                                                <p class="tg-pkgplanprice"><sup>RS</sup>500<sub>/per connect</sub> </p>
                                                <?php
                                                if(Auth::check()){
                                                if(Auth::user()->usertype!="user"){  ?>
                                                    {{-- <br/> <p><a href="{{  url('/logout') }}">Login</a> as a user to Get this offer</p> --}}
                                                    <a class="tg-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter1"><span>Get this Offer</span></a>
                                                <?php } else{
                                                ?>
                                                <a class="tg-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter1"><span>Get this Offer</span></a>
                                                <?php }}else{?>
                                                    <br/> <p><a href="{{  url('/register') }}">Register</a> as a user to Get this offer</p>
                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <div class="tg-pkgplan">
                                            <div class="tg-pkgplantitle">
                                                <h2>Plus</h2>
                                            </div>
                                            <ul class="tg-pkgplanoptions">
                                                <li>Book a Venue</li>
                                                <li>Book a car</li>
                                                <li>Decorate your Event</li>
                                            </ul>
                                                <div class="tg-pkgplanfoot text-center">
                                                    <p class="tg-pkgplanprice"><sup>RS</sup>2000<sub>/5 connects</sub> </p>
                                                    <?php
                                                    if(Auth::check()){
                                                    if(Auth::user()->usertype!="user"){  ?>
                                                        {{-- <br/> <p><a href="{{  url('/logout') }}">Login</a> as a user to Get this offer</p> --}}
                                                        <a class="tg-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter2"><span>Get this Offer</span></a>
                                                    <?php } else{
                                                    ?>
                                                    <a class="tg-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter2"><span>Get this Offer</span></a>
                                                    <?php }}else{?>
                                                        <br/> <p><a href="{{  url('/register') }}">Register</a> as a user to Get this offer</p>
                                                    <?php }?>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <div class="tg-pkgplan">
                                            <div class="tg-pkgplantitle">
                                                <h2>Premium</h2>
                                            </div>
                                            <ul class="tg-pkgplanoptions">
                                                <li>Book a Venue</li>
                                                <li>Book a car</li>
                                                <li>Decorate your Event</li>
                                            </ul>
                                                <div class="tg-pkgplanfoot text-center">
                                                    <p class="tg-pkgplanprice"><sup>RS</sup>3500<sub>/10 connects</sub> </p>
                                                    <?php
                                                    if(Auth::check()){
                                                    if(Auth::user()->usertype!="user"){  ?>
                                                        {{-- <br/> <p><a href="{{  url('/logout') }}">Login</a> as a user to Get this offer</p> --}}
                                                        <a class="tg-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter3"><span>Get this Offer</span></a>
                                                    <?php } else{
                                                    ?>
                                                    <a class="tg-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter3"><span>Get this Offer</span></a>
                                                    <?php }}else{?>
                                                        <br/> <p><a href="{{  url('/register') }}">Register</a> as a user to Get this offer</p>
                                                    <?php }?>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
     <!--************************************
				Footer Start
		*************************************-->
    <footer id="tg-footer" class="tg-footer tg-haslayout">
        <div class="tg-footerbar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p>Copyright &copy; 2022 Eventeous. All rights reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--************************************
				Footer End
		*************************************-->
    </div>
    <script src="{{ url('/') }}/assets/js/vendor/jquery-library.js"></script>
    <script src="{{ url('/') }}/assets/js/vendor/bootstrap.min.js"></script>
    <script src="{{ url('/') }}/assets/js/bootstrap-select.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery-scrolltofixed.js"></script>
    <script src="{{ url('/') }}/assets/js/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.mmenu.all.js"></script>
    <script src="{{ url('/') }}/assets/js/packery.pkgd.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.vide.min.js"></script>
    <script src="{{ url('/') }}/assets/js/scrollbar.min.js"></script>
    <script src="{{ url('/') }}/assets/js/prettyPhoto.js"></script>
    <script src="{{ url('/') }}/assets/js/countdown.js"></script>
    <script src="{{ url('/') }}/assets/js/parallax.js"></script>
    <script src="{{ url('/') }}/assets/js/main.js"></script>
    <script src="{{ url('/') }}/assets/login-register/sweetalert2@11.js"></script>
</body>
</html>


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Get a Booking Connect for PKR: 500/=</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <form id="subs-1">
                    <label style="text-align: center">Fields with * are required</label>
                    <input type="hidden" name="available_connects" value="1">
                    <input type="hidden" name="amount" value="500">
                    <input type="hidden" name="method" value="Visa">
                    <input type="hidden" name="user_id" value="<?=(isset(Auth::user()->id))? Auth::user()->id : '' ?>">
                    <div class="form-group">
                        <label for="inputAddress">Name on Card*</label>
                        <input  type="text" class="form-control" name="name_on_card" placeholder="Example: John Doe">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress2">Card Number<label>
                        <input type="text" maxlength="16"  class="form-control" name="card_number" placeholder="Example: 4012 8888 8888 1881">
                    </div>
                    <div style="width:45%;margin-right:10%;" class="form-group">
                        <label for="inputCity">Date of Expiry*</label>
                        <input maxlength="5" type="text" class="form-control" onkeyup="makeExpiryFormat(this)" placeholder="Example: 02/26" name="expiry">
                    </div>
                    <div style="width:45%;" class="form-group">
                        <label for="inputZip">CVV*</label>
                        <input maxlength="3" type="text" class="form-control" placeholder="Example: 021" name="cvv">
                    </div>
                </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="saveSubscriptions('subs-1')">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Get 5 Bookings Connects for PKR: 2000/=</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="subs-2">
                <label style="text-align: center">Fields with * are required</label>
                <input type="hidden" name="available_connects" value="5">
                <input type="hidden" name="amount" value="2000">
                <input type="hidden" name="method" value="Visa">
                <input type="hidden" name="user_id" value="<?=(isset(Auth::user()->id))? Auth::user()->id : '' ?>">
                <div class="form-group">
                    <label for="inputAddress">Name on Card*</label>
                    <input  type="text" class="form-control" name="name_on_card" placeholder="Example: John Doe">
                </div>
                <div class="form-group">
                    <label for="inputAddress2">Card Number<label>
                    <input type="text" maxlength="16"  class="form-control" name="card_number" placeholder="Example: 4012 8888 8888 1881">
                </div>
                <div style="width:45%;margin-right:10%;" class="form-group">
                    <label for="inputCity">Date of Expiry*</label>
                    <input maxlength="5" type="text" class="form-control" onkeyup="makeExpiryFormat(this)" placeholder="Example: 02/26" name="expiry">
                </div>
                <div style="width:45%;" class="form-group">
                    <label for="inputZip">CVV*</label>
                    <input maxlength="3" type="text" class="form-control" placeholder="Example: 021" name="cvv">
                </div>
            </form>
    </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="saveSubscriptions('subs-2')">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Get 10 Bookings Connects for PKR: 3500/=</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="subs-3">
                <label style="text-align: center">Fields with * are required</label>
                <input type="hidden" name="available_connects" value="10">
                <input type="hidden" name="amount" value="3500">
                <input type="hidden" name="method" value="Visa">
                <input type="hidden" name="user_id" value="<?=(isset(Auth::user()->id))? Auth::user()->id : '' ?>">
                <div class="form-group">
                    <label for="inputAddress">Name on Card*</label>
                    <input  type="text" class="form-control" name="name_on_card" placeholder="Example: John Doe">
                </div>
                <div class="form-group">
                    <label for="inputAddress2">Card Number<label>
                    <input type="text" maxlength="16"  class="form-control" name="card_number" placeholder="Example: 4012 8888 8888 1881">
                </div>
                <div style="width:45%;margin-right:10%;" class="form-group">
                    <label for="inputCity">Date of Expiry*</label>
                    <input maxlength="5" type="text" class="form-control" onkeyup="makeExpiryFormat(this)" placeholder="Example: 02/26" name="expiry">
                </div>
                <div style="width:45%;" class="form-group">
                    <label for="inputZip">CVV*</label>
                    <input maxlength="3" type="text" class="form-control" placeholder="Example: 021" name="cvv">
                </div>
            </form>
    </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="saveSubscriptions('subs-3')">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function saveSubscriptions(formId){
            var card_number = $("#"+formId+' input[name="card_number"]').val();
            var expiry = $("#"+formId+' input[name="expiry"]').val();
            var cvv = $("#"+formId+' input[name="cvv"]').val();
            if(card_number!="" && expiry!="" && cvv!=""){
                if(card_number.length<16 || cvv.length<3 || expiry.length<5){
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Invalid Card Details'
                    });
                }else{
                    $('#exampleModalCenter1').modal('hide');
                    $('#exampleModalCenter2').modal('hide');
                    $('#exampleModalCenter3').modal('hide');
                    let formData = new FormData($('#'+formId)[0]);
                    $.ajax({
                        type: 'post',
                        url:"{{ url('save-subscription') }}",
                        data:formData,
                        contentType:false,
                        processData:false,
                        success:function(data){
                            Swal.fire({
                            icon: 'success',
                            title: 'Good job!',
                            text: 'Connect Added Successfully'
                        });
                        location.reload();
                        $('#'+formId)[0].reset();
                        },
                    error: function (error) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Server Error'
                            })
                        }
                    });
                }

            }else{
                Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Fill out the required fields'
                    })
            }

        }

        makeExpiryFormat=(obj)=>{
            var value=$(obj).val();
            if(value.length>1 && value.includes('/')==false){
                $(obj).val(value+"/");
            }
        }
  </script>
  <style>
    .modal-backdrop{
        z-index:0;
    }

    .modal-backdrop.in {
        opacity: 0;
    }

    .modal-footer {
        padding: 15px;
        text-align: right;
        border-top:none !important;
    }

</style>
