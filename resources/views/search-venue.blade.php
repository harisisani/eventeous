<!doctype html>
<html class="no-js" lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Eventeous</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="{{ url('/') }}/assets/apple-touch-icon.png">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/normalize.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/icomoon.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/owl.carousel.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap-select.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/scrollbar.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/jquery.mmenu.all.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/prettyPhoto.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/transitions.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/color.css">
	<link rel="stylesheet" href="{{ url('/') }}/assets/css/responsive.css">
	<script src="{{ url('/') }}/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
		@include('header')

		<!--************************************
				Inner Banner Start
		*************************************-->
		<section class="tg-parallax tg-innerbanner" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ url('/') }}/assets/images/parallax/bgparallax-05.jpg">
			<div class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h1>Book Venue with Eventeous</h1>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--************************************
				Inner Banner End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="tg-main" class="tg-main tg-haslayout">
			<!--************************************
					Features Start
			*************************************-->
			<section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="tg-features">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="tg-feature">
								<div class="tg-featuretitle">
										<h2><span>01</span>Review</h2>
									</div>
									<div class="tg-description">
										<p>Review Details</p>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<div class="tg-feature">
									<div class="tg-featuretitle">
										<h2><span>02</span>Select</h2>
									</div>
									<div class="tg-description">
										<p>Enter your Booking Details</p>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<div class="tg-feature">
									<div class="tg-featuretitle">
										<h2><span>03</span>Book</h2>
									</div>
									<div class="tg-description">
										<p>Send Booking Request to Vendor</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
            @foreach($venue as $venue)
            <?php
                $days = (int) ((strtotime(now()) - strtotime($venue->created_at)) / (60 * 60 * 24));
                if($days>=1){
                    $posted="Posted: ".$days." day(s) ago";
                }else{
                $hours = (int) ((strtotime(now()) - strtotime($venue->created_at)) / (60 * 60));
                    $posted="Posted: ".$hours." hour(s) ago";
                }
            ?>
			<section class="tg-aboutus">
                <div class="container">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="row">
                            <figure data-vide-bg="poster: {{ url('/') }}{{'/uploads/venues/'.$venue->picture_links}}" data-vide-options="position: 0% 50%"></figure>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="tg-textbox">
                                <div class="tg-sectiontitle">
                                    <p><?php echo $posted ?></p>
                                    <h2>{{$venue->name}}</h2>
                                </div>
                                    <h4>
                                    <h4><strong>Price: </strong>{{$venue->price}}</h4>
                                    <h4><strong>City: </strong>{{$venue->city}}</h4>
                                    <h4><strong>Description: </strong>{{$venue->notes}}</h4>
                                    <?php
                                    $array = json_decode(json_encode($users), true);
                                        foreach($array as $user){
                                            if($user['id']==$venue->vendors_id){
                                                $vendor_name=$user['username'];
                                                $image=$user['image'];
                                            }
                                        }
                                    ?>
                                    <p>Posted by: <a style="text-decoration: none" href="{{ url('/') }}/view-profile/{{ $venue->vendors_id }}"><span><img class="rounded-circle" width="20px" src="{{ url('/') }}{{'/uploads/users/'.$image}}">&nbsp;{{ $vendor_name }}</span></a></p>
                                    <?php
                                    if(Auth::check()){
                                    if(Auth::user()->usertype!="user"){  ?>
                                        <br/> <p><a href="{{  url('/logout') }}">Login</a> as a user to Book this Venue</p>
                                    <?php } else{
                                    ?>
                                    <div class="tg-pkgplanfoot">
                                        <?php if(count($subs)>0 && $subs[0]['available_connects']>0){?>
                                            <p>Booking Connects Available: <?=$subs[0]['available_connects']?></p>
                                            <button style="float: right;" class="tg-btn" data-toggle="modal" onclick="openBooking('<?=$venue->vendors_id ?>','<?=$venue->id?>','<?=$venue->name?>');" data-target="#bookingModal">Book this venue</button>
                                        <?php }else{?>
                                            <p>No Bookings Connects Available | <a target="_blank" href="{{  url('/packages/') }}">Get Connects to book Venues</a></p>
                                        <?php }?>
                                    </div>
                                    <?php }}else{?>
                                        <br/> <p><a href="{{  url('/register') }}">Register</a> as a user to Book this Venue</p>
                                    <?php }?>
                            </div>
                        </div>
                    </div>
                    <div style="margin-top:50px; " class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="tg-description">{!! $venue->description !!}</div>
                        </div>
                    </div>
                    <div style="margin-top:50px; " class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="tg-sectiontitle">
                                <h2>Customer Reviews:</h2>
                            </div>
                        </div>
                        <?php
                        if(Auth::check()){
                        if(Auth::user()->usertype!="user"){  ?>
                            <br/> <p><a href="{{  url('/logout') }}">Login</a> as a user to add review for this Venue</p>
                        <?php } else{
                        ?>
                         <div style="margin-bottom: 25px;">
                            <div class="modal-footer">
                                <h4 style="text-align: left; float: left;"><strong>Add a Review for this venue</strong></h4><button type="button" class="btn btn-primary" onclick="saveReview('<?=$venue->id?>');">Submit</button>
                                </div>
                            <div class="form-floating mb-3">
                                <textarea class="form-control" name="reviewDescription" id="reviewDescription" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <?php }}else{?>
                            <br/> <p><a href="{{  url('/register') }}">Register</a> as a user to add review for this Venue</p>
                        <?php }?>
                        @foreach ($reviews as $review )
                        <table class="table">
                            <?php
                            $array = json_decode(json_encode($users), true);
                                foreach($array as $user){
                                    if($user['id']==$review->user_id){
                                        $name=$user['username'];
                                        $image=$user['image'];
                                    }
                                }
                            ?>
                            <tbody>
                              <tr>
                                <td style="text-align:left" scope="row"><a style="text-decoration: none" href="{{ url('/') }}/view-profile/{{ $venue->vendors_id }}"><img class="rounded-circle" width="50px" src="{{ url('/') }}{{'/uploads/users/'.$image}}">&emsp;<?=$name?></a></td>
                              </tr>
                              <tr>
                                <td style="text-align:left" scope="row">{!! $review->description !!}</td>
                              </tr>
                            </tbody>
                          </table>
                        @endforeach
                    </div>

                </div>
                </section>
            @endforeach
		</main>
		<!--************************************
				Main End
		*************************************-->
		<!--************************************
				Footer Start
		*************************************-->
		<footer id="tg-footer" class="tg-footer tg-haslayout">

			<div class="tg-footerbar">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p>Copyright &copy; 2022 Eventeous. All  rights reserved</p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--************************************
				Footer End
		*************************************-->
	</div>
	<!--************************************
			Wrapper End
	*************************************-->
	<!--************************************
			Search Start
	*************************************-->

	<script src="{{ url('/') }}/assets/js/vendor/jquery-library.js"></script>
	<script src="{{ url('/') }}/assets/js/vendor/bootstrap.min.js"></script>
	<script src="{{ url('/') }}/assets/js/bootstrap-select.min.js"></script>
	<script src="{{ url('/') }}/assets/js/jquery-scrolltofixed.js"></script>
	<script src="{{ url('/') }}/assets/js/owl.carousel.min.js"></script>
	<script src="{{ url('/') }}/assets/js/jquery.mmenu.all.js"></script>
	<script src="{{ url('/') }}/assets/js/packery.pkgd.min.js"></script>
	<script src="{{ url('/') }}/assets/js/jquery.vide.min.js"></script>
	<script src="{{ url('/') }}/assets/js/scrollbar.min.js"></script>
	<script src="{{ url('/') }}/assets/js/prettyPhoto.js"></script>
	<script src="{{ url('/') }}/assets/js/countdown.js"></script>
	<script src="{{ url('/') }}/assets/js/parallax.js"></script>
	<script src="{{ url('/') }}/assets/js/main.js"></script>
    <script src="{{ url('/') }}/assets/login-register/sweetalert2@11.js"></script>
    <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
</body>
</html>
<!-- Button trigger modal -->

  <!-- Modal -->
  <div class="modal fade" id="bookingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="saveBooking">
                <h4><strong>Enter Booking Date</strong></h4>
                <div class="form-floating mb-3">
                    <input type="date" style="margin-bottom:10px" class="form-control" name="bookingDate" id="bookingDate" cols="30" rows="10"></textarea>
                </div>
                <h4><strong>Enter Booking Description</strong></h4>
                <div class="form-floating mb-3">
                    <textarea class="form-control" name="bookingDescription" id="bookingDescription" cols="30" rows="10"></textarea>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" onclick="closeModal();" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="saveBooking();">Save changes</button>
        </div>
      </div>
    </div>
  </div>
<script>
    $(document).ready(function() {
        CKEDITOR.replace( 'bookingDescription' );
        CKEDITOR.replace( 'reviewDescription' );
    });

    var category='venue';
    var vendorId=0;
    var bookedId=0;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function openBooking(vendor_Id,booked_Id,name){
        vendorId=vendor_Id;
        bookedId=booked_Id;
        $('#exampleModalLabel').html("Enter Booking Details for "+name);
    }

    function saveReview(categoryId){
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var description=$('#reviewDescription').val();
        var booking = {category:category, categoryId:categoryId,description:description};
        $.ajax({
                type: 'post',
                url:"{{ url('save-review') }}",
                data:JSON.stringify(booking),
                contentType: 'application/json',
                dataType: 'json',
                success:function(data){
			   },
			   error: function (error) {
					// Swal.fire({
					// 	icon: 'error',
					// 	title: 'Oops...',
					// 	text: 'Server Error'
					// });
                    console.log("error");
                    console.log(error);
				}
			});
            location.reload();
            CKEDITOR.instances[instance].setData("");
    }

    function saveBooking(){
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var description=$('#bookingDescription').val();
        var date=$('#bookingDate').val();
        var booking = {vendor_id:vendorId, booked_id:bookedId,bookingDescription:description,bookingDate:date,booking_category:category};
        $.ajax({
                type: 'post',
                url:"{{ url('save-booking') }}",
                data:JSON.stringify(booking),
                contentType: 'application/json',
                dataType: 'json',
                success:function(data){

                    console.log(data);

			   },
			   error: function (error) {
					// Swal.fire({
					// 	icon: 'error',
					// 	title: 'Oops...',
					// 	text: 'Server Error'
					// });
                    console.log("error");
                    console.log(error);
				}
			});
            Swal.fire({
						icon: 'success',
						title: 'Good job!',
						text: 'booking request sent successfully'
					});
                    location.reload();
            $('#bookingModal').hide();
            CKEDITOR.instances[instance].setData("");
            $('#saveBooking')[0].reset();
    }

    function closeModal(){
        $('#bookingModal').hide();
    }

</script>
<style>
    .modal-backdrop{
        z-index:0;
    }

    .modal-backdrop.in {
        opacity: 0;
    }
</style>
