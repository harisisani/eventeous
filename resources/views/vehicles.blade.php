<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="zxx">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Eventeous</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{ url('/') }}/assets/apple-touch-icon.png">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/normalize.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/icomoon.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/bootstrap-select.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/scrollbar.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/jquery.mmenu.all.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/prettyPhoto.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/transitions.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/color.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/responsive.css">
    <script src="{{ url('/') }}/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body class="tg-home tg-homevone">
    @include('header')
    <div class="tg-bannerholder">
        <div class="tg-slidercontent">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h2>Search your desire with Eventeous</h2>
                        <form id="searchForm" enctype="multipart/form-data" action="{{ url('/search') }}"  method="POST" class="tg-formtheme tg-formtrip">
                            @csrf
                                <div style="width:25%;" class="form-group">
                                    <input type="text" name="searchKeyword" id="searchKeyword" style="width:100%;height: 50px;" placeholder="Keyword">
                                </div>
                                <div style="width:25%;" class="form-group">
                                    <div class="tg-select">
                                        <select class="selectpicker" name="searchCategory" id="searchCategory" data-live-search="true" data-width="100%">
                                            <option selected value="venues" >Venues</option>
                                            <option value="vehicles">Vehicles</option>
                                            <option value="decors">Decors</option>
                                        </select>
                                    </div>
                                </div>
                                <div style="width:25%;" class="form-group">
                                    <div class="tg-select">
                                        <select class="selectpicker" name="searchCity" id="searchCity" data-live-search="true" data-width="100%">
                                            <option selected value="Karachi">Karachi</option>
                                            <option value="Lahore">Lahore</option>
                                            <option value="Islamabad-Rawalpindi">Islamabad-Rawalpindi</option>
                                            <option value="Peshawar">Peshawar</option>
                                            <option value="Faisalabad">Faisalabad</option>
                                            <option value="Multan">Multan</option>
                                            <option value="Gujranwala">Gujranwala</option>
                                            <option value="Sialkot">Sialkot</option>
                                        </select>
                                    </div>
                                </div>
                                <div style="width:25%;" class="form-group">
                                    <button style="position:inherit;" class="tg-btn" type="submit">Search</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function doSearch(){
                $('#searchForm').submit();
                var formData = new FormData($('#searchForm')[0]);
                $.ajax({
                type: 'post',
                url:"{{ url('search') }}",
                data:formData,
                contentType:false,
                processData:false,
                success:function(data){

			   },
			   error: function (error) {

			}
			});
            }
        </script>
        <div id="tg-homeslider" class="tg-homeslider owl-carousel tg-haslayout">
            <figure class="item" style="max-height: 400px;min-height: 200px;" data-vide-bg="poster: {{ url('/') }}/assets/images/slider/img-01.jpg" data-vide-options="position: 0% 50%"></figure>
            <figure class="item" style="max-height: 400px;min-height: 200px;" data-vide-bg="poster: {{ url('/') }}/assets/images/slider/img-02.jpg" data-vide-options="position: 0% 50%"></figure>
            <figure class="item" style="max-height: 400px;min-height: 200px; " data-vide-bg="poster: {{ url('/') }}/assets/images/slider/img-03.jpg" data-vide-options="position: 0% 50%"></figure>
        </div>
    </div>
        <section style="padding:0px;" class="tg-sectionspace tg-haslayout">
            <div class="container">
                <div class="row">
                    @if (count($allVehicles) > 0)
                    <section class="tg-sectionspace tg-haslayout">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="tg-sectionhead tg-sectionheadvtwo">
                                        <div class="tg-sectiontitle">
                                            <h2>Vehicles</h2>
                                        </div>
                                        <div class="tg-description">
                                            <p>Book your vehicle now</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tg-trendingtrips">
                                    @foreach($allVehicles as $v)
                                    <?php
                                       $days = (int) ((strtotime(now()) - strtotime($v->created_at)) / (60 * 60 * 24));
                                       if($days>=1){
                                            $posted="Posted: ".$days." day(s) ago";
                                       }else{
                                        $hours = (int) ((strtotime(now()) - strtotime($v->created_at)) / (60 * 60));
                                            $posted="Posted: ".$hours." hour(s) ago";
                                       }
                                    ?>
                                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                        <div class="tg-trendingtrip">
                                            <div class="tg-description">
                                                <p><?php echo $posted ?></p>
                                            </div>
                                            <figure>
                                                <a href="{{ url('/vehicle/'.$v->id) }}">
                                                    <img style="height:250px;"  src="{{ url('/') }}{{'/uploads/vehicles/'.$v->picture_links}}" alt="image destinations">
                                                    <div class="tg-hover">
                                                        {{-- <span class="tg-stars"><span></span></span> --}}
                                                        {{-- <span class="tg-tourduration">24 hours</span> --}}
                                                        <span class="tg-locationname">{{$v->city}}</span>
                                                        <div class="tg-pricearea">
                                                            <span>from</span>
                                                            <h4>Rs {{$v->price}}</h4>
                                                        </div>
                                                    </div>
                                                </a>
                                            </figure>
                                            <div class="tg-populartourcontent">
                                                <div class="tg-populartourtitle">
                                                    <h3><a href="{{ url('/vehicle/'.$v->id) }}">{{$v->name}}</a></h3>
                                                </div>
                                                <div class="tg-description">
                                                    <p>{{$v->notes}}</p>
                                                </div>
                                                <div style="text-align: right;" class="tg-description">
                                                    <?php
                                                    $array = json_decode(json_encode($users), true);
                                                        foreach($array as $user){
                                                            if($user['id']==$v->vendors_id){
                                                                $vendor_name=$user['username'];
                                                            }
                                                        }
                                                    ?>
                                                    <p>Posted by: <a href="{{ url('/') }}/view-profile/{{ $v->vendors_id }}"><span>{{ $vendor_name }}</span></a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>


                            </div>
                        </div>
                    </section>
                    @endif
                </div>
            </div>
        </section>
    </main>
    <!--************************************
				Main End
		*************************************-->
    <!--************************************
				Footer Start
		*************************************-->
    <footer id="tg-footer" class="tg-footer tg-haslayout">
        <div class="tg-footerbar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p>Copyright &copy; 2022 Eventeous. All rights reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="{{ url('/') }}/assets/js/vendor/jquery-library.js"></script>
    <script src="{{ url('/') }}/assets/js/vendor/bootstrap.min.js"></script>
     <script src="{{ url('/') }}/assets/js/bootstrap-select.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery-scrolltofixed.js"></script>
    <script src="{{ url('/') }}/assets/js/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.mmenu.all.js"></script>
    <script src="{{ url('/') }}/assets/js/packery.pkgd.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.vide.min.js"></script>
    <script src="{{ url('/') }}/assets/js/scrollbar.min.js"></script>
    <script src="{{ url('/') }}/assets/js/prettyPhoto.js"></script>
    <script src="{{ url('/') }}/assets/js/countdown.js"></script>
    <script src="{{ url('/') }}/assets/js/parallax.js"></script>
    <script src="{{ url('/') }}/assets/js/main.js"></script>
</body>

</html>
