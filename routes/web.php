<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\VenuesController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\VehiclesController;
use App\Http\Controllers\DecorsController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\ConnectsController;
use App\Http\Controllers\ReviewsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/',[PageController::class,'index']);
Route::get('/home',[PageController::class,'index']);
Route::get('/about-us',[PageController::class,'aboutUs']);
Route::get('/contact-us',[PageController::class,'contactUs']);
Route::get('/packages',[PageController::class,'packages']);


Route::get('/view-profile/{id}',[PageController::class,'viewProfile']);
Route::get('/edit-profile',[PageController::class,'editProfile']);
Route::post('save-profile', [AuthController::class, 'saveProfile'])->name('save.profile');

Route::get('login', [AuthController::class, 'index'])->name('login');
Route::get('register', [AuthController::class, 'registration'])->name('register');
Route::post('post-login', [AuthController::class, 'postLogin'])->name('login.post');
Route::post('post-registration', [AuthController::class, 'postRegistration'])->name('register.post');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');
Route::post('user-delete', [AuthController::class, 'deleteUser'])->name('delete.user');

Route::get('dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');
Route::get('user_dashboard', [DashboardController::class, 'dashboard'])->name('user_dashboard');
Route::get('vendor_dashboard', [DashboardController::class, 'dashboard'])->name('vendor_dashboard');
Route::get('admin_dashboard', [DashboardController::class, 'dashboard'])->name('admin_dashboard');

//venue
Route::post('save-venue', [VenuesController::class, 'store']);
Route::get('/venue/{id}', [VenuesController::class, 'open_venue']);
Route::get('/venues', [VenuesController::class, 'open_all_venues']);
Route::post('delete-venue', [VenuesController::class, 'delete'])->name('delete.venue');
//vehicle
Route::post('save-vehicle', [VehiclesController::class, 'store']);
Route::get('/vehicle/{id}', [VehiclesController::class, 'open_vehicle']);
Route::get('/vehicles', [VehiclesController::class, 'open_all_vehicles']);
Route::post('delete-vehicle', [VehiclesController::class, 'delete'])->name('delete.vehicle');
//decors
Route::post('save-decor', [DecorsController::class, 'store']);
Route::get('/decor/{id}', [DecorsController::class, 'open_decor']);
Route::get('/decors', [DecorsController::class, 'open_all_decors']);
Route::post('delete-decor', [DecorsController::class, 'delete'])->name('delete.decor');

//booking
Route::post('save-booking', [BookingController::class, 'store']);
Route::post('booking-update', [BookingController::class, 'updateBookings'])->name('booking.update');

//search
Route::post('/search', [PageController::class, 'run_search']);

// subscription
Route::post('save-subscription', [ConnectsController::class, 'store']);

// reviews
Route::post('save-review', [ReviewsController::class, 'store']);
